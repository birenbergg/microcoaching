<?php 

$title = "microcoaching.net - on demand video game coaching";

$metaD = "win more games";

include("header.php"); 
?>
<body>



<section id="main-slider" class="no-margin">
        <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 1300px; height: 500px; overflow: hidden; visibility: hidden;">
        <!-- Loading Screen -->
          <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
              <div style="position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
              <div style="position:absolute;display:block;background:url('images/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
          </div>
          <div data-u="slides" style="cursor: default;top: 0px; left: 0px; width: 1300px; height: 500px; overflow: hidden;">
              <div style="display: none;position:static;" class="slide-loop">
                  <img data-u="image" src="images/homepage_top_image2.png" /> 
					<div class="front-drop"></div>
                  <div data-u="caption" class="slide-caption">
                    <!--<img src="images/banner_text_1.png" style="position: absolute; top: 0px; left: 0px; width: 530px;" />-->
					<div class="slide-content" style="background-color: rgba(0,0,0,0.6); padding:10px 0px 20px 0px; border-radius:20px; margin-top: 15px;">
						<h1 style="margin-top: 0px;">on demand video game coaching</h1>
						<p>The quickest and easiest way for gamers to get better. Get personalized help from an expert gamer and win more competitive games.</p>
						<div class="slide-btn"><a href="about.php"class="btn btn-secondary learn-more">Learn More</a>
<a href="https://game.microcoaching.net/Account/Login?ReturnUrl=%2FHome%2Fcoachingroom" class="btn btn-secondary find-coach">Find a Coach</a>
						<!--<button type="button" class="btn btn-secondary learn-more">Learn More</button>
						<button type="button" class="btn btn-secondary find-coach">Find a Coach</button>--></div>
					</div>
                  </div>
              </div>
              <!--<div style="display: none;">
                  <img data-u="image" src="images/homepage_top_image.jpg" />
                  <div data-u="caption" style="position: absolute; top: 50px; left: 700px; width: 530px;">
                    <img src="images/banner_text_1.png" style="position: absolute; top: 0px; left: 0px; width: 530px;" />
					<div class="slide-content">
						<h1>on demand video game coaching</h1>
						<p>The quickest and easiest way for gamers to get better. Get personalized help from an expert gamer now win more competitive games</p>
					</div>
                  </div>
              </div>
              <div style="display: none;"> 
                  <img data-u="image" src="images/homepage_top_image.jpg" />                 
                  <div data-u="caption" style="position: absolute; top: 50px; left: 700px; width: 530px;">
                    <img src="images/banner_text_1.png" style="position: absolute; top: 0px; left: 0px; width: 530px;" />
					<div class="slide-content">
						<h1>on demand video game coaching</h1>
						<p>The quickest and easiest way for gamers to get better. Get personalized help from an expert gamer now win more competitive games</p>
					</div>
                </div>
              </div>-->
          </div>
          <!-- Bullet Navigator -->
          <div data-u="navigator" class="jssorb05" style="bottom:16px;right:16px;" data-autocenter="1">
              <!-- bullet navigator item prototype -->
              <div data-u="prototype" style="width:16px;height:16px;"></div>
          </div>
          <!-- Arrow Navigator -->
          <span data-u="arrowleft" class="jssora22l" style="top:0px;left:12px;width:40px;height:58px;" data-autocenter="2"></span>
          <span data-u="arrowright" class="jssora22r" style="top:0px;right:12px;width:40px;height:58px;" data-autocenter="2"></span>
      </div>
    </section><!--/#main-slider-->
	<section id="games">
        <div class="container">
            <div class="games-slide">
              <div id="owl-demo">          
                <div class="item"><img src="images/games/lol.png" alt=""></div>
                <div class="item"><img src="images/games/overwatch.png" alt=""></div>
                <div class="item"><img src="images/games/counterstrike.png" alt=""></div>
                <div class="item"><img src="images/games/hearthstone.png" alt=""></div>
                <div class="item"><img src="images/games/dota2.png" alt=""></div> 
				<div class="item"><img src="images/games/sc2.png" alt=""></div> 				
              </div>			  
            </div>        
        </div><!--/.container-->
    </section><!--/#get started-->
	<div class="content-section">
			  <div class="container">
				<div class="testi-whole-section">
				<div class="col-md-4">
				  <div class="testimonial-wrapper">
					 <div class="col-content">
						<h5>Expert coaches</h5>
						<p>Learn from some of the most knowledgeable and friendly players in the game.</p>
						<a href="about.php" class="btn btn-secondary content-btn">Our Coaches</a>
						<!--<button type="button" class="btn btn-secondary content-btn">Our Coaches</button>-->
					  </div>
				  </div>
				</div>
				<div class="col-md-4">
					<div class="testimonial-wrapper">
					  
					  <div class="col-content">
						<h5>Easy to use</h5>
						<p>No delay, no setup screen Sharing, Built in voice and  video connectivity all in your browser with just a click.</p>
						<a href="about.php" class="btn btn-secondary content-btn">How It Works</a>
						<!--<button type="button" class="btn btn-secondary content-btn">How IT Works</button>-->
					  </div>
				  </div>
				</div>
				<div class="col-md-4">
					<div class="testimonial-wrapper">
					  
					  <div class="col-content">
						<h5>get good... fast</h5>
						<p>Stop Wasting time with YouTube tutorials, guides, Wikis and forums.</p>
					<a href="https://game.microcoaching.net/Account/SignUp" class="btn btn-secondary content-btn"> Sign Up</a>
						<!--<button type="button" class="btn btn-secondary content-btn">Sign Up</button>-->
					  </div>
				  </div>
				</div>
				</div>
			</div>
    </div>  
<!--	
	<div class="container">
		<div class="inner-content center">
			<h2>Coaching Sessions</h2>
			<p>Hundreds of coaching sessions! Check out a few short clips:</p>
		<div class="">	
		<div class="col-md-6 second-player">
			 <video style="padding-top:40px;width:100%;height:326px" id="player2" preload="none">
				  <source src="https://www.youtube.com/watch?v=AkWXfr3AEJg" type="video/youtube">
			 </video>				 
		</div>
		<div class="col-md-6 first-player">
			 <!--<iframe width="517" height="326" src="https://www.youtube.com/watch?v=APnl6XK7L9g"></iframe>
			 <video style="padding-top:40px;width:100%;height:326px" id="player1" preload="none">
				  <source type="video/youtube" src="https://www.youtube.com/watch?v=APnl6XK7L9g" >
			 </video>
		</div>
	
		</div>
		</div>
    </div>				  
<section>
<div  class="container" >
	 <div class="circle-cont" >
		<div class="board-members-block">
		  <div class="col-md-3">
			<div class="testi-img">
			  <img alt="Claire Pieper" class="img-responsive img-circle person_img" src="images/emre_aboutus1.jpg">
			</div>
			
		  </div>
		  <div class="col-md-9">
			<p class="tabbed-content" style="text-align: justify;">
				Obstacles don’t have to stop you. If you run into a wall, don’t turn around and give up. Figure out how to climb it, go through it, or work around it.
				<br><span>&#8211; Emre Ruhi, Coach</span>
			</p>
		  </div>
		  <div style="clear: both;"></div>
		</div>
	 </div>
</div>
</section>
-->
<!--<div class="push"></div>-->
    <?php 

include("footer.php"); 
?>
<!--old footer include-->
<!--
<footer class="footer" style="
/*bottom: 0px;*/
   /* position: fixed;*/
    /* margin-left: 0px; */
    /* margin-right: 0px; */
    width: 100%;
    z-index:0;">
<section class="footer-up">
  <div class="container" >
    <div class="try-section">Ready to give it a try? <div class="executive-box"><a href="https://game.microcoaching.net/Account/SignUp" class="btn btn-secondary sign-up">SIGN UP NOW!</a>
    <!--<button class="btn btn-secondary sign-up" type="button">SIGN UP NOW!</button>
    </div></div>
  </div>
  </section>




<div class="footer-down foot-bg">
		<div class="col-md-12">
           <p class="footer-text"><a href="http://www.twitter.com/microcoaching_">twitter</a> - <a href="http://www.facebook.com/microcoach">facebook</a> - <a href="mailto:coach@microcoaching.net">email</a></p>
           <p class="footer-text">copyright 2017 - <a href="javascript:;">microcoaching.net</a> - <a href="privacy.php">privacy policy</a></p>

		   </div>
  </div>
            <script>
$(".help").click(function() {
$(".fadebg").fadeToggle();
});
</script>

<script>
$(".fadebg").click(function() {
$(".fadebg").hide();
});
</script>
      </footer>


    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jssor.slider.mini.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/owl.carousel.js"></script>
	<script src="js/player.js"></script>
	<link  href="css/player.css" rel="Stylesheet" />
	<script>
		jQuery(document).ready(function($) {
			var player = new MediaElementPlayer('#player1');
			var player = new MediaElementPlayer('#player2');
		});
	</script>
    <script>
        jQuery(document).ready(function ($) {
            
            var jssor_1_SlideoTransitions = [
              [{b:5500,d:3000,o:-1,r:240,e:{r:2}}],
              [{b:-1,d:1,o:-1,c:{x:51.0,t:-51.0}},{b:0,d:1000,o:1,c:{x:-51.0,t:51.0},e:{o:7,c:{x:7,t:7}}}],
              [{b:-1,d:1,o:-1,sX:9,sY:9},{b:1000,d:1000,o:1,sX:-9,sY:-9,e:{sX:2,sY:2}}],
              [{b:-1,d:1,o:-1,r:-180,sX:9,sY:9},{b:2000,d:1000,o:1,r:180,sX:-9,sY:-9,e:{r:2,sX:2,sY:2}}],
              [{b:-1,d:1,o:-1},{b:3000,d:2000,y:180,o:1,e:{y:16}}],
              [{b:-1,d:1,o:-1,r:-150},{b:7500,d:1600,o:1,r:150,e:{r:3}}],
              [{b:10000,d:2000,x:-379,e:{x:7}}],
              [{b:10000,d:2000,x:-379,e:{x:7}}],
              [{b:-1,d:1,o:-1,r:288,sX:9,sY:9},{b:9100,d:900,x:-1400,y:-660,o:1,r:-288,sX:-9,sY:-9,e:{r:6}},{b:10000,d:1600,x:-200,o:-1,e:{x:16}}]
            ];
            
            var jssor_1_options = {
              $AutoPlay: false,
              $SlideDuration: 700,
              $SlideEasing: $Jease$.$OutQuint,
              $CaptionSliderOptions: {
                $Class: $JssorCaptionSlideo$,
                $Transitions: jssor_1_SlideoTransitions
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };
            
            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);
            
            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizing
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 1920);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
            ScaleSlider();
            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            //responsive code end
        });
$(document).ready(function(){
    $("#owl-demo").owlCarousel({   
        autoPlay: 3000, //Set AutoPlay to 3 seconds
        items : 6,
        itemsCustom : false,
        itemsDesktop : [1199,4],
        itemsDesktopSmall : [980,3],
        itemsTablet: [768,3],
        itemsTabletSmall: false,
        itemsMobile : [479,1] 
    });    
});
    </script> 
	-->
