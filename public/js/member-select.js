$('.team-member').click(function() {
    var memberName = $(this).attr('id');

    $('.team-member').removeClass('selected');
    $(this).addClass('selected');

    $('.member-info').hide();
    $('#' + memberName + '-info').show();
});