(function(){

	angular.module("mc").controller("promoCodesController", ['$scope', 'promoCodesService', '$filter', function ($scope, promoCodesService, $filter) {

		$scope.selectedCode = {};

		$scope.codes = [];

		$scope.showModal = function(code) {

			$scope.selectedCode = {};

			if (!angular.equals(code, {})) { // if promo code exists
				$scope.selectedCode.id = code.id;
				$scope.selectedCode.code = code.code;
				$scope.selectedCode.discount = code.discount;
			}
			document.getElementById('modal').classList.add('modal-open');
		}

		$scope.updateCode = function() {
			promoCodesService.updateCode($scope.selectedCode).then(function(response) {

				var code = $filter('filter')($scope.codes, {id: response.data.id}, true)[0];
				
				if (code === undefined) {
					$scope.codes.push(response.data);
				} else {
					code.id = response.data.id;
					code.code = response.data.code;
					code.discount = response.data.discount;
				}

				document.getElementById('modal').classList.remove('modal-open');
			});
		}

		$scope.deleteCode = function(code) {
			if(confirm("Are you sure?"))
			{
				promoCodesService.deleteCode(code).then(function() {
					var index = $scope.codes.indexOf(code);
					$scope.codes.splice(index, 1);
				});
			}
		}

		// When Escape pressed, close modal
		$(document).keypress(function(event) {
			if (event.keyCode === 27) {// esc
				$scope.hideModal();
			}
		});

		// Same as above, Chrome wrokaround
		$(document).keydown(function(event) {
			if (event.keyCode === 27) {// esc
				$scope.hideModal();
			}
		});

		$scope.hideModal = function() {
			document.getElementById('modal').classList.remove('modal-open');
		}
	}]);

}());