<?php 

$title = "microcoaching.net - on demand video game coaching";

$metaD = "win more games";

include("header-b.php"); 
?>


<!-- <body> -->

<!--chatlio script-->
<script type="text/javascript">
    window._chatlio = window._chatlio||[];
    !function(){ var t=document.getElementById("chatlio-widget-embed");if(t&&window.ChatlioReact&&_chatlio.init)return void _chatlio.init(t,ChatlioReact);for(var e=function(t){return function(){_chatlio.push([t].concat(arguments)) }},i=["configure","identify","track","show","hide","isShown","isOnline"],a=0;a<i.length;a++)_chatlio[i[a]]||(_chatlio[i[a]]=e(i[a]));var n=document.createElement("script"),c=document.getElementsByTagName("script")[0];n.id="chatlio-widget-embed",n.src="https://w.chatlio.com/w.chatlio-widget.js",n.async=!0,n.setAttribute("data-embed-version","2.1");
       n.setAttribute('data-widget-options', '{"embedInline": true}');
       n.setAttribute('data-widget-id','7311895d-b3c8-4f9e-6331-019b3318659f');
       c.parentNode.insertBefore(n,c);
    }();
</script>
<!--end chatlio script-->

<div class="jumbotron">
<div class="container jumbotron2">

 <div class="slide-content col-md-6 col-md-offset-3" style="background-color: rgba(0,0,0,0.6); padding:10px 0px 20px 0px; border-radius:20px; margin-top: 50px; margin-bottom: 50px;">
	<h1 style="margin-top: 10px;"><u>FREE</u> Advice from Expert League Coaches</h1>
	<div id="chatlioWidgetPlaceholder" style="margin:auto; height: 50vh; width: 85%;">
		
	</div>


	<!--<p>Get personalized help from an expert gamer NOW and win more competitive games.</p>
	<p><a class="btn btn-primary btn-lg learn-more" href="/sidekick" role="button">Try Now!</a></p>-->
						
 </div>
			
 
</div> <!--close container-->


</div><!--close jumbotron-->
<!--<div class="container-fluid col-md-10 col-md-offset-1" style="margin-bottom:20px;">
 <img src="/images/pr_landing.png" class="img-fluid" alt="PR for Microcoaching">
 </div>-->

	<div class="content-section" style="padding-top: 5px;padding-bottom: 5px;margin-bottom: 20px;margin-top: -20px;">
			  <div class="container">
			   <div style="width: 50%; text-align: center;" class="center-block">
			   <h4>Testimonials - Over 25,000 Messages Sent</h4>
			   </div>
				<div class="testi-whole-section">
		
				<div class="col-md-4">
				  <div class="testimonial-wrapper">
					 <div class="vcenter text-center">
						<p><i>"THANK YOU VERY MUCH I WON !!!"</i></p>
						<h5>-Hype League Player</h5>
						
						<!--<a href="about.php" class="btn btn-secondary content-btn">Our Coaches</a>-->
						<!--<button type="button" class="btn btn-secondary content-btn">Our Coaches</button>-->
					  </div>
				  </div>
				</div>
				<div class="col-md-4">
					<div class="testimonial-wrapper">
					  
					  <div class="vcenter text-center">
						<p><i>"i am not sure why i didn't do that before i never thought to do that"</i></p>
						<h5>-Knowledge Enthusiast</h5>
						
						<!--<a href="/sidekick.php" class="btn btn-secondary content-btn">Try Now</a>-->
						<!--<button type="button" class="btn btn-secondary content-btn">How IT Works</button>-->
					  </div>
				  </div>
				</div>
				<div class="col-md-4">
					<div class="testimonial-wrapper">
					  
					  <div class="vcenter text-center">
					<p><i>"this is sick"</i></p>
						<h5>-Smart League Player</h5>
						
					<!--<a href="https://game.microcoaching.net/Account/SignUp" class="btn btn-secondary content-btn"> Sign Up</a>-->
						<!--<button type="button" class="btn btn-secondary content-btn">Sign Up</button>-->
					  </div>
				  </div>
				</div>
				</div>
			</div>
    </div>  
	


	<section id="gameslist">
<!--<h2 class="h2-title2"> Supported Games: </h2>-->
<div class="row row-games">
  <div class="col-xs-2 col-md-2 thumbnail thumbnail-pr" >
    <img src="images/companies/dcinno.png" alt="dc inno">
  </div>
  <div class="col-xs-2 col-md-2 thumbnail thumbnail-pr">
    <img src="images/companies/incmagazine.png" alt="inc magazine - america's coolest college startups">
  </div>
  <div class="col-xs-2 col-md-2 thumbnail thumbnail-pr">
    <img src="images/companies/sofi.png" alt="sofi">
  </div>
  <div class="col-xs-2 col-md-2 thumbnail thumbnail-pr">
    <img src="images/companies/startuphoyas.png" alt="startup hoyas">
  </div>
  <div class="col-xs-2 col-md-2 thumbnail thumbnail-pr">
    <img src="images/companies/thehoya.png" alt="the hoya newspaper">
  </div>
  <div class="col-xs-2 col-md-2 thumbnail thumbnail-pr">
    <img src="images/companies/potomactechwire.png" alt="potomac tech wire">
  </div>
</div>
</section>



<!--info boxes-->
	<div class="content-section">
			  <div class="container">
				<div class="testi-whole-section">
				<div class="col-md-4">
				  <div class="testimonial-wrapper">
					 <div class="col-content vcenter">
						<h5>Expert coaches</h5>
						<p>Learn from some of the most knowledgeable and friendly players in the game.</p>
						<!--<a href="about.php" class="btn btn-secondary content-btn">Our Coaches</a>-->
						<!--<button type="button" class="btn btn-secondary content-btn">Our Coaches</button>-->
					  </div>
				  </div>
				</div>
				<div class="col-md-4">
					<div class="testimonial-wrapper">
					  
					  <div class="col-content vcenter">
						<h5>Easy to use</h5>
						<p>No delay, no setup. It's all in browser and totally free.</p>
						<!--<a href="/sidekick.php" class="btn btn-secondary content-btn">Try Now</a>-->
						<!--<button type="button" class="btn btn-secondary content-btn">How IT Works</button>-->
					  </div>
				  </div>
				</div>
				<div class="col-md-4">
					<div class="testimonial-wrapper">
					  
					  <div class="col-content vcenter">
						<h5>Get good... fast</h5>
						<p>Stop wasting time searching online for out-of-date & hard to find answers.</p>
					<!--<a href="https://game.microcoaching.net/Account/SignUp" class="btn btn-secondary content-btn"> Sign Up</a>-->
						<!--<button type="button" class="btn btn-secondary content-btn">Sign Up</button>-->
					  </div>
				  </div>
				</div>
				</div>
			</div>
    </div>  
	
	
	
	<section id="gameslist">
<!--<h2 class="h2-title2"> Supported Games: </h2>-->
<div class="row row-games">
  <div class="col-xs-2 col-md-2 thumbnail thumbnail-games" >
    <img src="images/games/lol.png" alt="league of legends">
  </div>
  <div class="col-xs-2 col-md-2 thumbnail thumbnail-games">
    <img src="images/games/overwatch.png" alt="overwatch">
  </div>
  <div class="col-xs-2 col-md-2 thumbnail thumbnail-games">
    <img src="images/games/counterstrike.png" alt="counterstrike">
  </div>
  <div class="col-xs-2 col-md-2 thumbnail thumbnail-games">
    <img src="images/games/hearthstone.png" alt="hearthstone">
  </div>
  <div class="col-xs-2 col-md-2 thumbnail thumbnail-games">
    <img src="images/games/dota2.png" alt="dota2">
  </div>
  <div class="col-xs-2 col-md-2 thumbnail thumbnail-games">
    <img src="images/games/sc2.png" alt="sc2">
  </div>
</div>
</section>
<!--	
	<div class="container">
		<div class="inner-content center">
			<h2>Coaching Sessions</h2>
			<p>Hundreds of coaching sessions! Check out a few short clips:</p>
		<div class="">	
		<div class="col-md-6 second-player">
			 <video style="padding-top:40px;width:100%;height:326px" id="player2" preload="none">
				  <source src="https://www.youtube.com/watch?v=AkWXfr3AEJg" type="video/youtube">
			 </video>				 
		</div>
		<div class="col-md-6 first-player">
			 <!--<iframe width="517" height="326" src="https://www.youtube.com/watch?v=APnl6XK7L9g"></iframe>
			 <video style="padding-top:40px;width:100%;height:326px" id="player1" preload="none">
				  <source type="video/youtube" src="https://www.youtube.com/watch?v=APnl6XK7L9g" >
			 </video>
		</div>
	
		</div>
		</div>
    </div>				  
<section>
<div  class="container" >
	 <div class="circle-cont" >
		<div class="board-members-block">
		  <div class="col-md-3">
			<div class="testi-img">
			  <img alt="Claire Pieper" class="img-responsive img-circle person_img" src="images/emre_aboutus1.jpg">
			</div>
			
		  </div>
		  <div class="col-md-9">
			<p class="tabbed-content" style="text-align: justify;">
				Obstacles don’t have to stop you. If you run into a wall, don’t turn around and give up. Figure out how to climb it, go through it, or work around it.
				<br><span>&#8211; Emre Ruhi, Coach</span>
			</p>
		  </div>
		  <div style="clear: both;"></div>
		</div>
	 </div>
</div>
</section>
-->
<!--<div class="push"></div>-->
    <?php 

include("footer-b.php"); 
?>
