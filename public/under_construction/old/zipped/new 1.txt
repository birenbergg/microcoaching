<div class="row">
  <div class="col-xs-2 col-md-2">
    
      <img class="thumbnail" src="images/games/lol.png" alt="...">
    
  </div>
  <div class="col-xs-2 col-md-2">
    <a href="#" class="thumbnail">
      <img src="images/games/overwatch.png" alt="...">
    </a>
  </div>
  <div class="col-xs-2 col-md-2">
    <a href="#" class="thumbnail">
      <img src="images/games/counterstrike.png" alt="...">
    </a>
  </div>
  <div class="col-xs-2 col-md-2">
    <a href="#" class="thumbnail">
      <img src="images/games/hearthstone.png" alt="...">
    </a>
  </div>
  <div class="col-xs-2 col-md-2">
    <a href="#" class="thumbnail">
      <img src="images/games/dota2.png" alt="...">
    </a>
  </div>
  <div class="col-xs-2 col-md-2">
    <a href="#" class="thumbnail">
      <img src="images/games/sc2.png" alt="...">
    </a>
  </div>
  
  
</div>
</section>



<section id="main-slider" class="no-margin">
        <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 1300px; height: 500px; overflow: hidden; visibility: hidden;">
        <!-- Loading Screen -->
          <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
              <div style="position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
              <div style="position:absolute;display:block;background:url('images/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
          </div>
          <div data-u="slides" style="cursor: default;top: 0px; left: 0px; width: 1300px; height: 500px; overflow: hidden;">
              <div style="display: none;position:static;" class="slide-loop">
                  <img data-u="image" src="images/homepage_top_image2.png" /> 
					<div class="front-drop"></div>
                  <div data-u="caption" class="slide-caption">
                    <!--<img src="images/banner_text_1.png" style="position: absolute; top: 0px; left: 0px; width: 530px;" />-->
					<div class="slide-content" style="background-color: rgba(0,0,0,0.6); padding:10px 0px 20px 0px; border-radius:20px; margin-top: 15px;">
						<h1 style="margin-top: 0px;">FREE Advice from Expert Gamers</h1>
						<p>Get personalized help from an expert gamer NOW and win more competitive games.</p>
						<div class="slide-btn"><a href="chat.php" class="btn btn-secondary learn-more">Try Now!</a>
						<!--<button type="button" class="btn btn-secondary learn-more">Learn More</button>
						<button type="button" class="btn btn-secondary find-coach">Find a Coach</button>--></div>
					</div>
                  </div>
              </div>
              <!--<div style="display: none;">
                  <img data-u="image" src="images/homepage_top_image.jpg" />
                  <div data-u="caption" style="position: absolute; top: 50px; left: 700px; width: 530px;">
                    <img src="images/banner_text_1.png" style="position: absolute; top: 0px; left: 0px; width: 530px;" />
					<div class="slide-content">
						<h1>on demand video game coaching</h1>
						<p>The quickest and easiest way for gamers to get better. Get personalized help from an expert gamer now win more competitive games</p>
					</div>
                  </div>
              </div>
              <div style="display: none;"> 
                  <img data-u="image" src="images/homepage_top_image.jpg" />                 
                  <div data-u="caption" style="position: absolute; top: 50px; left: 700px; width: 530px;">
                    <img src="images/banner_text_1.png" style="position: absolute; top: 0px; left: 0px; width: 530px;" />
					<div class="slide-content">
						<h1>on demand video game coaching</h1>
						<p>The quickest and easiest way for gamers to get better. Get personalized help from an expert gamer now win more competitive games</p>
					</div>
                </div>
              </div>-->
          </div>
          <!-- Bullet Navigator -->
          <div data-u="navigator" class="jssorb05" style="bottom:16px;right:16px;" data-autocenter="1">
              <!-- bullet navigator item prototype -->
              <div data-u="prototype" style="width:16px;height:16px;"></div>
          </div>
          <!-- Arrow Navigator -->
          <span data-u="arrowleft" class="jssora22l" style="top:0px;left:12px;width:40px;height:58px;" data-autocenter="2"></span>
          <span data-u="arrowright" class="jssora22r" style="top:0px;right:12px;width:40px;height:58px;" data-autocenter="2"></span>
      </div>
    </section><!--/#main-slider-->
	<section id="games">
        <div class="container">
            <div class="games-slide">
              <div id="owl-demo">          
                <div class="item"><img src="images/games/lol.png" alt=""></div>
                <div class="item"><img src="images/games/overwatch.png" alt=""></div>
                <div class="item"><img src="images/games/counterstrike.png" alt=""></div>
                <div class="item"><img src="images/games/hearthstone.png" alt=""></div>
                <div class="item"><img src="images/games/dota2.png" alt=""></div> 
				<div class="item"><img src="images/games/sc2.png" alt=""></div> 				
              </div>			  
            </div>        
        </div><!--/.container-->
    </section><!--/#get started-->