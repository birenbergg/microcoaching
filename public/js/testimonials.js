var testimonials;

function fadeItIn(timeout) {
    $('#testimonial-container').fadeIn(1000, function () {
        setTimeout(function () {
            fadeItOut();
        }, timeout < 2000 ? 2000 : timeout);
    });
}

function fadeItOut() {
    $('#testimonial-container').fadeOut(1000, function () {
        var rnd = Math.floor(Math.random() * testimonials.length);
        $('#testimonial').html(testimonials[rnd - 1]);
        fadeItIn(testimonials[rnd - 1].length * 100);
    });
}

$(document).ready(function() {
    $.ajax({
        type: "GET",
        url: "testimonials.csv",
        dataType: "text",
        success: function(data) {
            testimonials = data.split(/\r\n|\n/);
            fadeItOut();
        }
    });
});