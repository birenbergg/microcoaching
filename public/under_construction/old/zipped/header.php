<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="description" content="<?php

if(isset($metaD) && !empty($metaD)) { 
   echo $metaD; 
} 
else { 
   echo "on demand video game coaching, league of legends coaching, csgo coaching"; 
} ?>" />

<title><?php 
if(isset($title) && !empty($title)) { 
   echo $title; 
} 
else { 
   echo "Microcoaching.net - On Demand Video Game Coaching"; 
} ?></title>

	<!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <!-- <link href="css/bootstrapTheme.css" rel="stylesheet"> -->
<!--<link href="css/custom.css" rel="stylesheet">-->
<link href="css/custom2.css" rel="stylesheet">
    <!--<link href="css/owl.carousel.css" rel="stylesheet">-->
    <!--<link href="css/owl.theme.css" rel="stylesheet">-->

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->   
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<link rel='shortcut icon' type='image/x-icon' href='/favicon.ico' />
</head><!--/head-->



 

<header id="header">
    <div class="header-wrapper">
          <nav class="navbar navbar-inverse" role="banner">
              <div class="container" style="width:100%; padding-right:0px; padding-left:0px">
			    <div class="header-menu">
				  <div class="navbar-header">
				  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				  <span class="sr-only">Toggle navigation</span>
				  <span class="icon-bar"></span>
				  <span class="icon-bar"></span>
				  <span class="icon-bar"></span>
				  </button>
				  <div class="logo-box"><a class="navbar-brand" href="index.php"><img src="images/header_logo2.png" alt="logo" height="80px"></a></div>
				  </div>
				  <div class="collapse navbar-collapse navbar-right mymenu">
				  <ul class="nav navbar-nav">
			<li><a href="http://www.twitter.com/microcoaching_" style="margin-right:10px; font-size:16px; margin-top:1px;"><i class="fa fa-twitter" aria-hidden="true"></i>
			
			<li><a href="http://www.facebook.com/microcoach" style="margin-right:10px; font-size:16px; margin-top:1px;"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
          
          <li><a href="about.php" style="margin-right:10px; font-size:16px; margin-top:1px;">ABOUT</a></li>
		  <li><a href="about.php" style="margin-right:10px; font-size:16px; margin-top:1px;">TOOLS</a></li>
          <li><a href="contactform.php" style="margin-right:15px; font-size:16px; margin-top:1px;">BECOME A COACH</a></li>

				  <!--dropdown insert start-->
         <!-- <li class="dropdown">
              <a href="about.php" class="dropdown-toggle" data-toggle="dropdown">ABOUT<span class="caret"></span></a>
                <ul class="dropdown-menu">
              <li><a href="about.php">OUR TEAM</a></li>
                    <li><a href="http://www.microcoaching.net/our-coaches.html">OUR COACHES</a></li>
                    <li><a href="contactform.php">BECOME A COACH</a></li>
                </ul>
          </li> -->
          <!--dropdown insert end-->
          <!--<li><a href="faq.php">FAQ</a></li>-->
     
				  <li><a href="/chat.php" class="btn btn-secondary login" style="font-size:22px;">TRY NOW!</a><li>
				  </ul>
				  </div>
				</div>
              </div> 
          </nav>
    </div>

<!--fb message-->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=247767469007773";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!--end fb msg-->
</header>

<!---end of headerphp-->