<footer>
    @if(!Auth::user() && !Route::is('login') && !Route::is('register') && !Request::is('password/reset') && !Route::is('admin.dashboard'))
    <div id="signup">
        <div>Ready to give it a try?</div>
        <a href="{{ route('register') }}">Sign Up Now</a>
    </div>
    @endif

    <div id="newsletter">
        <div id="mc_embed_signup">
            <form action="//microcoaching.us13.list-manage.com/subscribe/post?u=4f5b24e3f801d6d00d0d5814b&amp;id=ac2e39040a" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                <div id="mc_embed_signup_scroll">
                    <div class="mc-field-group">
                        <label for="mce-EMAIL">Stay Updated </label>
                        <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email Address" required>
                        <input type="submit" value="Let's Go!" name="subscribe" id="mc-embedded-subscribe" class="button">
                    </div>
                    <div id="mce-responses" class="clear">
                        <div class="response" id="mce-error-response" style="display:none"></div>
                        <div class="response" id="mce-success-response" style="display:none"></div>
                    </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                </div>
            </form>
        </div>
    </div>

    <div id="links">
        <div id="social">
            <a href="//twitter.com/microcoaching_" target="_blank"><i class="fab fa-twitter"></i></a>
            <a href="//www.facebook.com/microcoach/" target="_blank"><i class="fab fa-facebook-f"></i></a>
            <a href="//www.linkedin.com/company/11046852/" target="_blank"><i class="fab fa-linkedin-in"></i></a>
            <a href="//www.twitch.tv/microcoaching" target="_blank"><i class="fab fa-twitch"></i></a>
            <a href="//www.youtube.com/channel/UCgjlaTBEmijBWke8VAs__dg" target="_blank"><i class="fab fa-youtube"></i></a>
        </div>
        <div id="external">
            <a href="{{ route('privacy') }}">Privacy Policy</a> | <a href="{{ route('terms') }}">Terms of Use</a>
        </div>
        <div style="font-size: 12px; text-align: center; margin-top: 2em; max-width: 1200px;">
            <p>No endorsement is express or implied. Any third-party game marks are mentioned solely to clarify compatibility with Microcoaching’s coaching services. Overwatch and Hearthstone are trademarks and/or registered trademarks of Blizzard Entertainment Inc. Counter-Strike and Dota are trademarks and/or registered trademarks of the Valve Corporation. Microcoaching Inc. and its services are not endorsed by Riot Games and does not reflect the views or opinions of Riot Games or anyone officially involved in producing or managing League of Legends. League of Legends and Riot Games are trademarks or registered trademarks of Riot Games, Inc. Any other marks are trademarks and/or registered trademarks of their respective owners.</p>
        </div>
    </div>
    
    <div id="copy">
        &copy; <?php echo date('Y') >= 2018 ? date('Y') : '2018&ndash;' . date('Y'); ?> Microcoaching Inc. All Rights Reserved
</footer>

@yield('scripts')