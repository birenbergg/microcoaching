<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="{{ asset('css/mail.css') }}">
</head>
<body>
    <div id="wrapper">
        <h1>New Message</h1>
        <div id="content">
            <div class="group">
                <label>Name</label>
                <div class="text">{{ $msg->name }}</div>
            </div>

            <div class="group">
                <label>Email</label>
                <div class="text">{{ $msg->email }}</div>
            </div>

            <div class="group">
                <label>Phone Number</label>
                <div class="text">{{ ($msg->phone != null) ? $msg->phone : '(not specified)' }}</div>
            </div>

            <div class="group">
                <label>Birth Date</label>
                <div class="text">{{ ($msg->birthdate != null) ? $msg->birthdate : '(not specified)' }}</div>
            </div>

            <div class="group">
                <label>Subject</label>
                <div class="text">{{ ($msg->subject != null) ? $msg->subject : '(not specified)' }}</div>
            </div>
            
            <div class="group">
                <label>Message</label>
                <div class="text">{{ $msg->message }}</div>
            </div>
        </div>
    </div>
</body>
</html>
