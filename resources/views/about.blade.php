@extends('layout.master')
@section('title', 'About')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/about.css') }}">
@stop

@section('scripts')
    <script src="//code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="{{ asset('js/member-select.js') }}"></script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-115361507-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-115361507-1');
	</script>
@stop

@section('content')
    <section id="our-story" class="message">
        <h3>Our Story</h3>
        <div class="message-body">
            <p>Microcoaching was founded in 2016 by a team of lifelong gamers and friends. Our mission is to make it easier for gamers of all skill levels to get better by having access to the help they want, when they want it.</p>
            <p>Your coaching sessions include on-demand chats and personalized gaming experiences, and our platform helps gamers improve by connecting them to experts in real-time. We offer instant private chat, personalized video coaching sessions and tons of exclusive content to help you reach your goals.</p>
            <p>Want to start winning more? Or just have a quick question? Get an answer right away! Ask your coach whatever you want to know, tell them what problems you are having or what goals/rank/level you want to achieve. Our excellent coaches use a wide range of tools and strategies to get you where you want to be quickly and effectively. No matter your question, our coaches are always here to help. If you are stuck in a game, want to improve or want to win more, sign up and get a real coach—right away.</p>
        </div>
    </section>

    <section id="team">
        <h3>Meet Our Team</h3>
        <div id="team-container">
            <div class="team-member-container">
                <div id="dan" class="team-member">
                    <div class="member-image">
                        <img src="images/team/dan.jpg" alt="Dan">
                    </div>
                    <div class="caption">
                        <p class="name">Dan</p>
                        <p class="title">CEO</p>
                    </div>
                </div>
            </div>
            <div class="team-member-container">
                <div id="aya" class="team-member">
                    <div class="member-image">
                        <img src="images/team/aya.jpg" alt="Aya">
                    </div>
                    <div class="caption">
                        <p class="name">Aya</p>
                        <p class="title">CTO</p>
                    </div>
                </div>
            </div>
            <div class="team-member-container">
                <div id="yehonatan" class="team-member">
                    <div class="member-image">
                        <img src="images/team/yehonatan.jpg" alt="Yahonatan">
                    </div>
                    <div class="caption">
                        <p class="name">Yehonatan</p>
                        <p class="title">COO</p>
                    </div>
                </div>
            </div>
        </div>

        <div id="dan-info" class="message member-info">
            <h4>Dan Tasch &ndash; Chief Executive Officer</h4>
            <p>An original Founder and our Leader, Dan has been with Microcoaching from the very start.  Breaking the corporate chains, Dan left his practice as a patent attorney to pursue Microcoaching’s mission and live every gamers dream. A BS in Mechanical Engineering from UMD, JD from Cardozo and an MBA from Gtown, Dan’s most treasured credential: FIFA Dorm Champion—and he never misses a chance to remind his friends of it.</p>
        </div>

        <div id="aya-info" class="message member-info">
            <h4>Aya Lev &ndash; Chief Technology Officer</h4>
            <p>Microcoaching’s technology lead, Aya is our architect and optimizer. As a compiler engineer for Ceva, a Nasdaq-traded company, she participated in exploration and R&D to help the company launch next-gen services. Also originally from Israel, Aya earned a BS in Computer Science from Tel Aviv University and was a flight simulator instructor in the Israeli Air Force – her specializations included the Skyhawk and Bell 206. She has always been a huge fan of Tekken and NBA Street.</p>
        </div>

        <div id="yehonatan-info" class="message member-info">
            <h4>Yehonatan Reut &ndash; Chief Operating Officer</h4>
            <p>Our in-house Strategy and Ops Expert, Yehonatan joined the team in January 2018 adding a tech start-up veteran to the roster.  The serial entrepreneur worked for the fast-growing cyber security company, Protected Media, and managed compliance operations for Media Group, an online marketing firm. Originally from Israel, Yehonatan was Head of Special Field Training in the IDF Special Forces. His games of choice: GTA III and Red Alert 2—keeping it old school!</p>
        </div>

    </section>

    <section id="achievements">

        <a class="achievement" href="//www.instagram.com/p/BMUmaXSgOZ9/" target="_blank">
            <div class="circle">
                <div>
                    Winners SoFi Pitch Competition & Class participants
                </div>
                <div>
                    - - - - - - - - - - 
                </div>
                <div>
                    October 2016
                </div>
            </div>
            <img src="{{ asset('images/companies/sofi_logo.jpg') }}" alt="SoFi">
        </a>

        <a class="achievement" href="//www.inc.com/college-startups" target="_blank">
            <div class="circle">
                <div>
                    One of Inc. Magazine’s Coolest College Srartups
                </div>
                <div>
                    - - - - - - - - - - 
                </div>
                <div>
                    March 2016
                </div>
            </div>
            <img src="{{ asset('images/companies/Inc-Magazine-Logo.jpg') }}" alt="Inc">
        </a>

        <a class="achievement" href="//msb.georgetown.edu/newsroom/news/summer-incubator-hosts-student-alumni-entrepreneurs" target="_blank">
            <div class="circle">
                <div>
                    Finalists Georgetown StartupHoyas Challenge
                </div>
                <div>
                    - - - - - - - - - - 
                </div>
                <div>
                    April 2016
                </div>
            </div>
            <img src="{{ asset('images/companies/startuphoyas.png') }}" alt="Inc">
        </a>

        <a class="achievement" href="//mava.org/programs/techbuzz/spring-2017/#startups" target="_blank">
            <div class="circle">    
                <div>
                    Finalists Leonsis Family Enterperneurship Prize
                </div>
                <div>
                    - - - - - - - - - - 
                </div>
                <div>
                    November 2016
                </div>
            </div>
            <img src="{{ asset('images/companies/TB_Spring_2017.png') }}" alt="Inc">
        </a>

        <a class="achievement" href="//www.americaninno.com/dc/meet-the-georgetown-startup-for-professional-gamers-and-their-parents/" target="_blank">
            <div class="circle">    
                <div>
                    Top 64 Tech Company in Dc by DC Inno
                </div>
                <div>
                    - - - - - - - - - - 
                </div>
                <div>
                    February 2017
                </div>
            </div>
            <img src="{{ asset('images/companies/DCINNO.png') }}" alt="DC Inno">
        </a>

        <a class="achievement" href="//www.thehoya.com/georgetown-students-start-video-game-tutoring-company/" target="_blank">
            <div class="circle">    
                <div>
                    Winners Georgetown’s Seed Investment Practicum
                </div>
                <div>
                    - - - - - - - - - - 
                </div>
                <div>
                    December 2016
                </div>
            </div>
            <img src="{{ asset('images/companies/thehoya.png') }}" alt="The Hoya">
        </a>

    </section>
@stop