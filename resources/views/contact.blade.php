@extends('layout.master')
@section('title', 'Contact')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/contact.css') }}">
@stop

@section('scripts')
	<script src="//code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="{{ asset('js/testimonials.js') }}"></script>
    
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-115361507-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-115361507-1');
	</script>
@stop

@section('content')
    <section id="contact-links">
        <a id="youtube-link" href="//www.youtube.com/channel/UCgjlaTBEmijBWke8VAs__dg" target="_blank">
            <i class="fab fa-youtube"></i>
            <div>
                follow our coaches
            </div>
        </a>
        <a id="discord-link" href="https://discordapp.com/invite/GnpGuE5" target="_blank">
            <i class="fab fa-discord"></i>
            <div>
                chat with us
            </div>
        </a>
        <a id="twitter-link" href="//twitter.com/microcoaching_" target="_blank">
            <i class="fab fa-twitter"></i>
            <div>
                check out cool promos
            </div>
        </a>
        <a id="facebook-link" href="//www.facebook.com/microcoach/" target="_blank">
            <i class="fab fa-facebook-f"></i>
            <div>
                like our page
            </div>
        </a>
        <a id="twitch-link" href="//www.twitch.tv/microcoaching" target="_blank">
            <i class="fab fa-twitch"></i>
            <div>
                watch our streamers
            </div>
        </a>
        <a id="linkedin-link" href="//www.linkedin.com/company/11046852/" target="_blank">
            <i class="fab fa-linkedin-in"></i>
            <div>
                business inquiries
            </div>
        </a>
    </section>

    <section id="contact-message" class="message">
        <h3>Our Support Team Is Dedicated to You!</h3>
        <div class="message-body">
            <p>We are here to solve any issue and provide you with the best and fastest solution.</p>
            <p class="emph">Can’t find the right coach? Forgot your password? Having trouble with the video app?</p>
        </div>
    </section>
    
    <section id="contact-form-section" class="message">
        <h3>Reach Out and Continue Playing While We Take Care of You!</h3>
        <form id="contact-form" method="post" action="{{ route('contact.post') }}">
            {{ csrf_field() }}

            <div style="grid-area: name;">
                <input type="text" name="name" placeholder="Your Name">
                @if ($errors->has('name'))
                    <small class="error">{{ $errors->first('name') }}</small>
                @endif
            </div>

            <div style="grid-area: email;">
                <input type="email" name="email" placeholder="Email Address">
                @if ($errors->has('email'))
                    <small class="error">{{ $errors->first('email') }}</small>
                @endif
            </div>

            <div style="grid-area: phone;">
                <input type="text" name="phone" placeholder="Phone Number">
                @if ($errors->has('phone'))
                    <small class="error">{{ $errors->first('phone') }}</small>
                @endif
            </div>

            <div style="grid-area: date;">
                <input type="text" name="birthdate" placeholder="Date of Birth &ndash; Special Promotions on Your Special Day">
                @if ($errors->has('birthdate'))
                    <small class="error">{{ $errors->first('birthdate') }}</small>
                @endif
            </div>

            <input type="text" name="subject" placeholder="Subject &ndash; Technical/Payment/Coach/General Inquiry (optional)" style="grid-area: subject;">
            
            <div style="grid-area: message;">
                <textarea name="message" id="" cols="30" rows="10" placeholder="How can we help you today?"></textarea>
                @if ($errors->has('message'))
                    <small class="error">{{ $errors->first('message') }}</small>
                @endif
            </div>
            
            <input type="submit" value="Submit Now" style="grid-area: submit;">
        </form>
    </section>
@stop