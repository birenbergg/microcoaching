</body>
<footer class="footer" style="bottom: 0px;
    position: fixed;
    /* margin-left: 0px; */
    /* margin-right: 0px; */
    width: 100%;
    z-index:0;">
<section class="footer-up">
  <div class="container" >
    <div class="try-section">Ready to give it a try? <div class="executive-box"><a href="https://game.microcoaching.net/Account/SignUp" class="btn btn-secondary sign-up">SIGN UP NOW!</a>
    <!--<button class="btn btn-secondary sign-up" type="button">SIGN UP NOW!</button>-->
    </div></div>
  </div>
  </section>




<div class="footer-down foot-bg">
		<div class="col-md-12">
           <p class="footer-text"><a href="http://www.twitter.com/microcoaching_">twitter</a> - <a href="http://www.facebook.com/microcoach">facebook</a> - <a href="mailto:coach@microcoaching.net">email</a></p>
           <p class="footer-text">copyright 2017 - <a href="javascript:;">microcoaching.net</a> - <a href="privacy.php">privacy policy</a> - patent pending</p>

		   </div>
  </div>
            <script>
$(".help").click(function() {
$(".fadebg").fadeToggle();
});
</script>

<script>
$(".fadebg").click(function() {
$(".fadebg").hide();
});
</script>
      </footer>


    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jssor.slider.mini.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/owl.carousel.js"></script>
	<script src="js/player.js"></script>
	<link  href="css/player.css" rel="Stylesheet" />
	<script>
		jQuery(document).ready(function($) {
			var player = new MediaElementPlayer('#player1');
			var player = new MediaElementPlayer('#player2');
		});
	</script>
    <script>
        jQuery(document).ready(function ($) {
            
            var jssor_1_SlideoTransitions = [
              [{b:5500,d:3000,o:-1,r:240,e:{r:2}}],
              [{b:-1,d:1,o:-1,c:{x:51.0,t:-51.0}},{b:0,d:1000,o:1,c:{x:-51.0,t:51.0},e:{o:7,c:{x:7,t:7}}}],
              [{b:-1,d:1,o:-1,sX:9,sY:9},{b:1000,d:1000,o:1,sX:-9,sY:-9,e:{sX:2,sY:2}}],
              [{b:-1,d:1,o:-1,r:-180,sX:9,sY:9},{b:2000,d:1000,o:1,r:180,sX:-9,sY:-9,e:{r:2,sX:2,sY:2}}],
              [{b:-1,d:1,o:-1},{b:3000,d:2000,y:180,o:1,e:{y:16}}],
              [{b:-1,d:1,o:-1,r:-150},{b:7500,d:1600,o:1,r:150,e:{r:3}}],
              [{b:10000,d:2000,x:-379,e:{x:7}}],
              [{b:10000,d:2000,x:-379,e:{x:7}}],
              [{b:-1,d:1,o:-1,r:288,sX:9,sY:9},{b:9100,d:900,x:-1400,y:-660,o:1,r:-288,sX:-9,sY:-9,e:{r:6}},{b:10000,d:1600,x:-200,o:-1,e:{x:16}}]
            ];
            
            var jssor_1_options = {
              $AutoPlay: false,
              $SlideDuration: 700,
              $SlideEasing: $Jease$.$OutQuint,
              $CaptionSliderOptions: {
                $Class: $JssorCaptionSlideo$,
                $Transitions: jssor_1_SlideoTransitions
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };
            
            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);
            
            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizing
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 1920);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
            ScaleSlider();
            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            //responsive code end
        });
$(document).ready(function(){
    $("#owl-demo").owlCarousel({   
        autoPlay: 3000, //Set AutoPlay to 3 seconds
        items : 6,
        itemsCustom : false,
        itemsDesktop : [1199,4],
        itemsDesktopSmall : [980,3],
        itemsTablet: [768,3],
        itemsTabletSmall: false,
        itemsMobile : [479,1] 
    });    
});
    </script>

</html>