@extends('layout.admin-master')
@section('title', 'Promo Codes')

@section('styles')
    @parent <link rel="stylesheet" type="text/css" href="/css/modal.css">
@stop

@section('scripts')
    <script src="//code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular-cookies.js"></script>
    
    <script src="/js/app.js"></script>
    <script src="/js/pc-service.js"></script>
    <script src="/js/pc-controller.js"></script>
@stop

@section('ng-controller', 'promoCodes')

@section('admin-content')
    <section ng-init="codes = {{ $codes }}">
        <h2>Promo Codes</h2>
        
        <table>
            <thead>
                <tr>
                    <th>Code</th>
                    <th>Discount</th>
                </tr>
            </thead>
            <tbody>
                <tr ng-repeat="code in codes">
                    <td>@{{ code.code }}</td>
                    <td>@{{ code.discount }}</a></td>
                    <td style="text-align: right;">
                        <button ng-click="showModal(code)">Edit</button>
                        <button style="background: red" ng-click="deleteCode(code)">Delete</button>
                    </td>
                </tr>
            </tbody>
        </table>

        <button ng-click="showModal({})">Add</button>

        <!-- Modal -->
        <div id="modal" class="modal-wrapper">
            <div class="modal">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-title">@{{ selectedCode.id == undefined ? 'Add' : 'Edit' }} Promo Code</h2>
                    </div>
                    <div class="modal-body">
                        <div>
                            <div class="label">Code:</div>
                            <div class="input-wrapper"><input id="code" ng-model="selectedCode.code" value="@{{ selectedCode.code }}" /></div>
                        </div>
                        <div>
                            <div class="label">Discount:</div>
                            <div class="input-wrapper"><input id="credits" ng-model="selectedCode.discount" value="@{{ selectedCode.discount }}" /></div>
                        </div>
                    </div>
                    <div id="modal-buttons-wrapper">
                        <button type="button" ng-click="updateCode()" class="normal">Save</button>
                        <button type="button" ng-click="hideModal()" class="cancel">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop