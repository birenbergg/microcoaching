<?php 

$title = "faq - microcoaching.net - on demand video game coaching";

$metaD = "win more games - rank upleague of legends csgo hearthstone";

include("header.php"); 
?>
    <div class="bag-img">
    <div class="banner-section container">
        <h1 class="text-center">FAQ</h1>
    </div>
    </div>
<div class="container">
    <div class="faq-cnt">
        <p>What is Microcoaching?</p>
        <span style="margin-bottom:30px; margin-top:-5px">On-demand coaching for video game players. If you're stuck in a game, want to improve or want to win more, sign up and get a real coach, right away.</span>
<br><br>
        <p>How does it work?</p>
        <span style="margin-bottom:30px; margin-top:-5px">It's easy, just click "try now" up top and start your session - it's free!</span>
<br><br>
        <p>How much does it cost?</p>
        <span style="margin-bottom:30px; margin-top:-5px">Free!</span>
<br><br>
        <p>Who are the coaches?</p>
        <span style="margin-bottom:30px; margin-top:-5px">Highly experienced, expert players and pros </span>
<br><br>
		<p>What's a session like?</p>
        <span style="margin-bottom:30px; margin-top:-5px">Your coaching session is an on-demand chat and gaming experience with your coach. Ask him whatever your want to know, tell him what problems you're having or what goals/rank/level you want to achieve. Our excellent coaches use a wide range of tools and strategies to get you where you want to be quickly and effectively.</span>
<br><br>
        <p>Contact Info?</p>
        <span style="margin-bottom:30px; margin-top:-5px">Email us at <a href="mailto:coach@microcoaching.net">coach@microcoaching.net</a>, message on <a href="www.facebook.com/microcoach">facebook</a>, or hit us up on <a href="www.twitter.com/microcoach_">twitter</a></span>
<br><br>
        <p>I want to coach - where do I apply?</p>
        <span style="margin-bottom:30px; margin-top:-5px">So you want to make money and help other gamers? Awesome! <a href="/apply.php">Click here to start your application</a>. We'll be in touch ASAP.
        </span>
       
    </div>
</div>
<!--
    <section class="row col-user">
        <div class="container">
        <div class="col-md-12">
            <div class="col-md-4 about-testi">
                <img src="images/emre_aboutus1.jpg" class="img-circle" width="250px">
                <h1>emre ruhi</h1>
                <p><b>Team Fortress:</b> Demoman extraordinare; <b>Starcraft:</b> Diamond League Zergling rusher</p>
            </div>
            <div class="col-md-4 about-testi">
                <img src="images/dwight_aboutus2.jpg" class="img-circle" width="250px">
                <h1>dwight stalls</h1>
                <p><b>The Sims 3 Pets:</b> Competitive equestrian; <b>MLB 2K16:</b> Furious couch casual</p>
            </div>
            <div class="col-md-4 about-testi">
                <img src="images/dan_aboutus3.jpg" class="img-circle" height="243px" width="243px">
                <h1>dan tasch</h1>
                <p><b>Law Simulator 2016:</b> Level III Grand Wizard; <b>Pokemon Go:</b> Level 25 (32OOCP Pikachu)</p>
            </div>
        </div>
        </div>
    </section>
<section class="our-story container">
    <div class="container">
        <h1 class="text-center">our story</h1>
        <p>Microcoaching was founded in Spring 2016 by three MBA students at Georgetown University: Emre, Dwight and Dan.</p><br>
        <p>They've been playing games, competitively and casually, for decades. They like to win, so when microcoaching started winning startup competitions, they knew they had something truly OP.</p>
    </div>
</section>
<section class="bottom-cnt">
<div class="container">
    <section>
        <div class="with-us">
            <h1 class="text-center">who's with us?</h1>
            <p>Our mentors and partners include some of the most inﬂuential VCs and entrepreneurs on the east coast.</p>
            <div class="col-md-12 img-section">
            <div class="col-md-4 with-us-logo">
                <img src="images/startuphoyas.png">
            </div>
            <div class="col-md-4 with-us-logo">
                <img src="images/1776.jpg">
            </div>
            <div class="col-md-4 with-us-logo">
                <img src="images/georgetown.jpg">
            </div>
        </div>
            <p>Want to learn more or work with our team?</p>
            <button type="button" class="btn btn-secondary contact-us">CONTACT US</button>
        </div>
    </section>
</div>
</section>-->

<?php include("footer.php"); ?>