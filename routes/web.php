<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::view('/', 'home')->name('home');
Route::view('/home', 'home')->name('home-home');
Route::view('/about', 'about')->name('about');

Route::get('/become-coach', 'BecomeCoachController@get')->name('bac');
Route::post('/become-coach', 'BecomeCoachController@post')->name('bac.post');

Route::view('/privacy-policy', 'privacy')->name('privacy');
Route::view('/terms-of-use', 'terms')->name('terms');

Route::get('/contact', 'ContactController@get')->name('contact');
Route::post('/contact', 'ContactController@post')->name('contact.post');

Route::get('/chat', 'ChatController@text')->name('chat-text');
Route::get('/video-chat', 'ChatController@video')->name('chat-video');

Route::post('/chat/onecreditdown/{id}', 'ChatController@oneCreditDown')->name('onecreditdown');

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
	Route::get('/packages', 'PackagesController@index')->name('packages');
	Route::get('/packages/{slug}', 'PackagesController@package')->name('package');
	Route::get('/packages/{slug}/promo', 'PackagesController@packagePromo')->name('packagePromo');
	Route::get('/payment-process/{paymentID}/{payerID}/{token}/{packageID}/{promoCode?}', 'PackagesController@paymentProcess');
	
	Route::view('/router', 'router')->name('router');

	Route::get('/purchase-history', 'ProfileController@purchaseHistory')->name('purchase-history');
});

Route::group(['middleware' => ['auth', 'admin']], function () {
	Route::get(env('ADMIN_DASHBOARD'), 'AdminController@users')->name('dashboard');
	
	Route::get(env('ADMIN_DASHBOARD').'/users', 'AdminController@users')->name('admin.users');
	Route::post('/users/update/{id}', 'AdminController@updateUser')->name('user.update');

	Route::get(env('ADMIN_DASHBOARD').'/packages', 'AdminController@packages')->name('admin.packages');
	Route::post('/packages/update/{id?}', 'AdminController@updatePackage')->name('packages.update');
	Route::get('/packages/delete/{id}', 'AdminController@deletePackage')->name('packages.delete');

	Route::get(env('ADMIN_DASHBOARD').'/promo-codes', 'AdminController@promoCodes')->name('admin.promo-codes');
	Route::post('/promo-codes/update/{id?}', 'AdminController@updatePromoCode')->name('promo-codes.update');
	Route::delete('/promo-codes/delete/{id}', 'AdminController@deletePromoCode')->name('promo-codes.delete');

	Route::get(env('ADMIN_DASHBOARD').'/purchases', 'AdminController@purchases')->name('admin.purchases');
});

Route::get('/login/facebook', 'SocialAuthFacebookController@redirect');
Route::get('/login/facebook/callback', 'SocialAuthFacebookController@callback');

Route::get('/login/google', 'SocialAuthGoogleController@redirect');
Route::get('/login/google/callback', 'SocialAuthGoogleController@callback');