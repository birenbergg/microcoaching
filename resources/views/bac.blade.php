@extends('layout.master')
@section('title', 'Become A Coach')
@section('styles')
    <link rel="stylesheet" href="{{ asset('css/bac.css') }}">
@stop
@section('scripts')
<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-115361507-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-115361507-1');
	</script>
@stop
@section('content')
    <section id="bac-message" class="message">
        <h3>Think You Have What It Takes to Be a Coach?</h3>
        <div class="message-body">
            <p>Want to start making money for playing video games? Want to work on your own schedule? Microcoaching is growing quickly and we need more coaches! Complete our application below if you think you have what it takes. We’ll be in touch soon with selected applicants. We are fully committed to supporting our coaches and assisting them in any way to promote and market their own endeavors through our platform.</p>
            <p>Want to help other gamers and get paid for your expertise?</p>
            <p>Fill out the form below. We'll get back to you ASAP!</p>
        </div>
    </section>
    
    <section id="bac-form-section" class="message">
        <h3>Please Give Us Your Info to Join</h3>

            <form action="{{ route('bac.post') }}" method="post">
                {{ csrf_field() }}
                <div class="bac-form">
                    <input type="text" name="name" placeholder="Your Name" required style="grid-area: name;">
                    <input type="email" name="email" placeholder="Email Address" required style="grid-area: email;">
                    <input type="email" name="skype_discord" placeholder="Skype / Discord" required style="grid-area: skype;">
                    <input type="text" name="phone" placeholder="Phone Number (optional)" style="grid-area: phone;">
                    <input type="text" name="country_timezone" placeholder="Country / Timezone" required style="grid-area: country;">
                    <input type="text" name="age" placeholder="Age (optional)" style="grid-area: age;">
                    <input type="text" name="steam" placeholder="Steam Account Name" required style="grid-area: steam;">
                    <!-- <input type="text" placeholder="What game(s) do you want to coach?" required style="grid-area: games;"> -->
                    
                    <select required style="grid-area: games;" name="game">
                        <option selected value="">What game(s) do you want to coach?</option>
                        <option value="League of Legends">League of Legends</option>
                        <option value="Counter-Strike: Global">Counter-Strike: Global Offensive</option>
                        <option value="Overwatch">Overwatch</option>
                        <option value="Hearthstone">Hearthstone</option>
                        <option value="DOTA 2">DOTA 2</option>
                        <option value="Other - Please specify below">Other - Please specify below</option>
                    </select>

                    <textarea name="gaming_xp" id="" cols="30" rows="10" placeholder="Please describe your experience / level / rank in the game(s) you selected above" required style="grid-area: exp;"></textarea>
                    <textarea name="teaching_xp" id="" cols="30" rows="10" placeholder="Do you have any teaching experience? If so please describe... (optional)" style="grid-area: teaching;"></textarea>
                    <input name="opgg" type="text" placeholder="Your OP.GG Link" style="grid-area: opgg;">
                    <input name="twitter" type="text" placeholder="Your Twitter Link (optional)" style="grid-area: twitter;">
                    <input name="twitch" type="text" placeholder="Your Twitch Link (optional)" style="grid-area: twitch;">
                    <input name="youtube" type="text" placeholder="Your Youtube Link (optional)" style="grid-area: youtube;">

                    <select name="availability" required style="grid-area: hours;">
                        <option selected value="">How many hours are you available to coach per week?</option>
                        <option value="1-2 hours">1-2 hours</option>
                        <option value="2-4 hours">2-4 hours</option>
                        <option value="4-6 hours">4-6 hours</option>
                        <option value="6-10 hours">6-10 hours</option>
                        <option value="10+ hours">10+ hours</option>
                    </select>
<!--
                    <input type="text" placeholder="I agree to Microcoaching's Terms and Conditions and Privacy Policy" required style="grid-area: agreement;">
                    -->
                    <input type="submit" value="Submit Now" style="grid-area: submit;">
                </div>
            </form>
    </section>
@stop