(function(){

	angular.module("mc").controller("usersController", ['$scope', 'usersService', '$filter', function ($scope, usersService, $filter) {

		$scope.selectedUser = {};

		$scope.users = [];

		$scope.showModal = function(user) {

			$scope.selectedUser.id = user.id;
			$scope.selectedUser.name = user.name;
			$scope.selectedUser.credits = user.credits;

			document.getElementById('modal').classList.add('modal-open');
		}

		$scope.updateUser = function() {
			usersService.updateUser($scope.selectedUser).then(function() {

				var user = $filter('filter')($scope.users, {id: $scope.selectedUser.id}, true)[0];
				
				user.credits = $scope.selectedUser.credits;

				document.getElementById('modal').classList.remove('modal-open');
			});
		}

		// $(document).click(function(event) {
		// 	$scope.hideModal();
		// });

		// When Escape pressed, close modal
		$(document).keypress(function(event) {
			if (event.keyCode === 27) {// esc
				$scope.hideModal();
			}
		});

		// Same as above, Chrome wrokaround
		$(document).keydown(function(event) {
			if (event.keyCode === 27) {// esc
				$scope.hideModal();
			}
		});

		$scope.hideModal = function() {
			document.getElementById('modal').classList.remove('modal-open');
		}
	}]);

}());