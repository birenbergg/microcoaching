<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="google-site-verification" content="2tJjRpcZLeUlbZKxeTxMYK63DewOrQc9E92Exh3caKs" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>@yield('title')</title>
    
    <link href="//fonts.googleapis.com/css?family=Montserrat|Roboto+Condensed" rel="stylesheet">
    <link href="//use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
    
    <link rel="icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon"/>

    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/common.css') }}" />
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/header.css') }}" />
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/footer.css') }}" />

    <link rel="stylesheet" media="screen and (max-width: 1200px)" href="{{ asset('css/max-width-1200.css') }}">
    <link rel="stylesheet" media="screen and (max-width: 992px)" href="{{ asset('css/max-width-992.css') }}">
    <link rel="stylesheet" media="screen and (max-width: 768px)" href="{{ asset('css/max-width-768.css') }}">
    <link rel="stylesheet" media="screen and (max-width: 600px)" href="{{ asset('css/max-width-600.css') }}">

    @yield('styles')
</head>

<body ng-app="mc" ng-controller="@yield('ng-controller')Controller">
    @include('layout.header')

    <main>
        @yield('content')        
    </main>

    @include('layout.footer')
</body>
</html>