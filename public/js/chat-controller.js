(function(){

	angular.module("mc").controller("chatController", ['$scope', '$timeout', '$interval', '$cookies', 'chatService', function ($scope, $timeout, $interval, $cookies, chatService) {

        $scope.user;
        $scope.online;
        $scope.answeredOnce = $cookies.get('answeredOnce');

        oneCreditDown = () => {
            chatService.oneCreditDown($scope.user.id).then(function() {
                $scope.user.credits--;
            });
        }

        $scope.setAnsweredOnce = () => {
            if ($scope.answeredOnce !== 1) {
                oneCreditDown();
            }

            $scope.answeredOnce = 1;
            $cookies.put('answeredOnce', 1);
        }

        $scope.stopCounter = ()=> {
            $cookies.put('answeredOnce', 0);
            $scope.answeredOnce = 0;
        }

        $interval(() => {
            if ($scope.online && $scope.answeredOnce == 1) {
                if ($scope.user.credits > 0) {
                    oneCreditDown();
                } else {
                    document.location.href = '/router';
                }
            }
        }, 600000); // every ten minutes
	}]);

}());