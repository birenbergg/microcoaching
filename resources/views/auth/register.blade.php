@extends('layout.master')
@section('title', 'Register')

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/auth.css') }}">
@stop

@section('scripts')
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script>
        $('a.button.disabled').click(function(event) {
            event.preventDefault();
        });

        $('#tac').change(function() {
            var isChecked = $(this).is(':checked');
            if(isChecked) {
                $('input[type=submit]').prop('disabled', false);
            } else {
                $('input[type=submit]').prop('disabled', true);
            }
        });
    </script>
@stop

@section('content')
<section>
    <h1>Register to Microcoaching</h1>
	<p>Sign up now to get your 2 FREE Credits. No strings attached.</p>
    <div class="auth-form-wrapper">
        <form method="POST" action="{{ route('register') }}" class="auth-form register">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <div class="col-md-6">
                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required placeholder="Name">

                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <div class="col-md-6">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="Email Address">

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <div class="col-md-6">
                    <input id="password" type="password" class="form-control" name="password" required placeholder="Password">

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Confirm Password">
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6">
                    <label for="tac">
                        <input id="tac" type="checkbox" class="form-control" name="tac" required>
                        I agree to the <a href="{{ route('terms') }}">Terms and Conditions </a> and <a href="{{ route('privacy') }}">Privacy Policy </a> . 
                    </label>
                </div>
            </div>

            <input type="submit" value="Register" disabled>

            @include('auth.social')
        </form>
    </div>

    <h2>Already have an account?</h2>
    <a href="{{ route('login') }}">Login</a>
</section>
@endsection
