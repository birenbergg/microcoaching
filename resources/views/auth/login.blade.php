@extends('layout.master')
@section('title', 'Login')

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/auth.css') }}">
@stop

@section('content')
<section>
    <h1>Login to Microcoaching</h1>
    <div class="auth-form-wrapper">
        <form method="POST" action="{{ route('login') }}" class="auth-form">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <div class="col-md-6">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="Email Address">

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <div class="col-md-6">
                    <input id="password" type="password" class="form-control" name="password" required placeholder="Password">

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="remember-forgot">
                <label>
                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                </label>
                <a href="{{ route('password.request') }}">
                    Forgot Your Password?
                </a>
            </div>

            <input type="submit" value="Login">

            @include('auth.social')
        </form>
    </div>
    <h2>Don't have an account?</h2>
	<p>Sign up now to get your 2 FREE Credits. No strings attached.</p>
    <a href="{{ route('register') }}">Create one now</a>
</section>
@endsection
