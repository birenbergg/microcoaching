@extends('layout.master')
@section('title', 'Packages')
@section('styles')
    <link rel="stylesheet" href="{{ asset('css/packages.css') }}">
@stop
@section('scripts')
    <script src="//code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="{{ asset('js/testimonials.js') }}"></script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-115361507-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-115361507-1');
	</script>

@stop
@section('content')
    <section class="header">
        <h2>Find Your League Coach Today!</h2>
        <p>Special launch prices!</p>
    </section>

    <section id="packages-container">
        @foreach($packages as $package)
            <div class="package">
                <div class="top">
                    <div>
                        <h4>{{ $package->name }}</h4>
                        <p class="price">${{ $package->price }}</p>
                        <hr>
                        <p class="credits">{{ $package->credits_amount }} credits</p>
                    </div>
                </div>
                <div class="bottom">
                    
                        <a class="buy" href="/packages/{{ $package->slug }}">Buy Now</a>
                    
                </div>
            </div>

        @endforeach
    </section>
    
    <section id="packages-message" class="message">
        <h3>Use Your Credits Any Way You Want</h3>
        <div class="message-body">
            <div>
                <div>1 credit =</div>
                <div>
                    <img src="{{ asset('images/instant-chat.svg') }}" alt="">
                    <div>10 minutes of Private Text with an Expert Coach</div>
                </div>
            </div>
            <div>
                <div>10 credits =</div>
                <div>
                    <img src="{{ asset('images/video-coaching.svg') }}" alt="">
                    <div>60 minutes of Video Coaching with Seamless Screen Sharing</div>
                </div>
            </div>
        </div>
    </section>
@stop