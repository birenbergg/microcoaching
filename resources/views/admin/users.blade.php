@extends('layout.admin-master')
@section('title', 'Users')

@section('styles')
    @parent <link rel="stylesheet" type="text/css" href="/css/modal.css">
@stop

@section('scripts')
    <script src="//code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular-cookies.js"></script>

    <script src="/js/app.js"></script>
    <script src="/js/u-service.js"></script>
    <script src="/js/u-controller.js"></script>
@stop

@section('ng-controller', 'users')

@section('admin-content')
    <section ng-init="users = {{ $users }}">
        <h2>Users</h2>

    	<table>
    		<thead>
    			<tr>
    				<th>Name</th>
    				<th>Email</th>
    				<th>Credits</th>
                    <th></th>
    			</tr>
    		</thead>
    		<tbody>
                <tr ng-repeat="user in users">
                    <td>@{{ user.name }}</td>
                    <td>@{{ user.email }}</td>
                    <td>@{{ user.credits }}</td>
                    <td style="text-align: right;">
                        <button ng-click="showModal(user)">Edit Credits</button>
                    </td>
                </tr>
    		</tbody>
    	</table>

        <!-- Modal -->
        <div id="modal" class="modal-wrapper">
            <div class="modal">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-title">Update credits for @{{ selectedUser.name }}</h2>
                    </div>
                    <div class="modal-body">
                        <div>
                            <div class="label">Credits:</div>
                            <div class="input-wrapper"><input id="credits" ng-model="selectedUser.credits" value="@{{ selectedUser.credits }}" /></div>
                        </div>
                    </div>
                    <div id="modal-buttons-wrapper">
                        <button type="button" ng-click="updateUser()" class="normal">Save</button>
                        <button type="button" ng-click="hideModal()" class="cancel">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop