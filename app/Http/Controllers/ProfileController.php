<?php

namespace App\Http\Controllers;

use Auth;
use App\CreditPurchase;

class ProfileController extends Controller
{
    public function purchaseHistory()
    {
        $purchases = CreditPurchase::where('user_id', Auth::user()->id)->get();
        return view('purchase-history', ['purchases' => $purchases]);
    }
}
