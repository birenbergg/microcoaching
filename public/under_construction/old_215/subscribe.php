<?php 
$title = "stay updated with microcoaching.net - on demand video game coaching";

$metaD = "win more games - video game coaching - get better in league of legends csgo hearthstone";

include("header.php"); 
?>

<!-- <body> -->
<div class="container center" style="position:relative; z-index: 999;">
<h2>Stay updated & in the loop! We're growing quickly and constantly adding cool new features and tools for video game players:</h2>
    
<!-- Begin MailChimp Signup Form -->
<link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet" type="text/css">
<style type="text/css">
	#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; width:100%; display:inline;}
	/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
	   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
</style>
<div id="mc_embed_signup" style="display:inline;">
<form action="//microcoaching.us13.list-manage.com/subscribe/post?u=4f5b24e3f801d6d00d0d5814b&amp;id=fcc3d87e8e" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll" style="display:inline;">
	<!--<label for="mce-EMAIL" style="display:inline;">Stay Updated!</label>-->
	<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_4f5b24e3f801d6d00d0d5814b_fcc3d87e8e" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Let's Go!" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
    </div>
</form>
</div>
<!--End mc_embed_signup-->
    
<h3>we'll never share, sell or spam your email</h3>

</div>
<?php include("footersub.php"); ?>
