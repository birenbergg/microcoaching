<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mc_blog');

/** MySQL database username */
define('DB_USER', 'mcblog');

/** MySQL database password */
define('DB_PASSWORD', 'asdfASDF1');

/** MySQL hostname */
define('DB_HOST', 'localhost:3306');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '5R4hR3oiTW2KM^l%U^c7(bl9b^Pko&djbLlfGmsjSsPqqnpR2pBoiCCFuER4&I9V');
define('SECURE_AUTH_KEY',  'swO29P8xkjXVWBzxffw&*eIiHBGP2jDP%(ni3Mm6H)nxr)i226%hbpEA6ShlDaVI');
define('LOGGED_IN_KEY',    'FaZgRVjf&RPT2MW0bRXXbpUl05GTF*0^ust))T7K1%kLK5xeLY*9AtdtaA6vMCzH');
define('NONCE_KEY',        'Bj^ePpdyWb0%uCXcd!q0VQUlTatiaZWAGDEeOglkYbJLL@VA0QGkuq6DTOPC)m%8');
define('AUTH_SALT',        ')Z2@dz9k#F31A^dK8eDo6Ts99xGiPVS1Wms9UWe1PA7hE!W8Kq!&fU1rdUZTzcoi');
define('SECURE_AUTH_SALT', 'BytthU)Ew00y21!ZhWzbu#iAUjI4VZWFqOLhktdEVR7@fUjatB56jP%GnfTGhB%a');
define('LOGGED_IN_SALT',   'qEa0AK*^ZTMe)6HeQhW4V3yjeCfVGDQ1J@Sw323dMi2IHVrfxq^0j&Ke*t99i3o#');
define('NONCE_SALT',       'Yb%JMmluWMKvn*si%#I5lTp59nRuJsVMa4mJ^E&WfOLTDUhy)^p4iI!snDtw2mmT');
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'Y8nFe_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define( 'WP_ALLOW_MULTISITE', true );

define ('FS_METHOD', 'direct');
?>