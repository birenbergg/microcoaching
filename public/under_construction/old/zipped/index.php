<?php 

$title = "microcoaching.net - on demand video game coaching";

$metaD = "win more games";

include("header.php"); 
?>
<body>
<div class="jumbotron">
<div class="container jumbotron2">
 <div class="slide-content col-md-6 col-md-offset-3" style="background-color: rgba(0,0,0,0.6); padding:10px 0px 20px 0px; border-radius:20px; margin-top: 100px; margin-bottom: 100px;">
	<h1 style="margin-top: 10px;">FREE Advice from Expert Gamers</h1>
	<p>Get personalized help from an expert gamer NOW and win more competitive games.</p>
	<p><a class="btn btn-primary btn-lg learn-more" href="#" role="button">Try Now!</a></p>
						
 </div>
			
  
</div> <!--close container-->

</div><!--close jumbotron-->

<div class="content-section">
<h2 class="h2-title"> As seen on: </h2>
<div class="row row-sites">
  <div class="col-xs-3 col-md-3"> 
 <a href="#" class="thumbnail"> 
    <img src="http://www.microcoaching.net/uploads/7/5/4/3/75438927/published/inc-logo.png?1489764469" alt="...">
	</a>
  </div>
  <div class="col-xs-3 col-md-3">
    <a href="#" class="thumbnail"> 
	<img src="http://www.microcoaching.net/uploads/7/5/4/3/75438927/editor/hoya-logo.png?1488535085" alt="...">
  </a>
  </div>
  <div class="col-xs-3 col-md-3">
    <a href="#" class="thumbnail"> 
	<img src="http://www.microcoaching.net/uploads/7/5/4/3/75438927/dcinno-logo_orig.jpg" alt="...">
</a> 
 </div>
  <div class="col-xs-3 col-md-3">
<a href="#" class="thumbnail">    
   <img src="http://www.microcoaching.net/uploads/7/5/4/3/75438927/editor/potomactechwire-logo_1.jpg?1488535088" alt="...">
  </a>
  </div>

</div>
</div>

<section id="gameslist">
<!--<h2 class="h2-title2"> Supported Games: </h2>-->
<div class="row row-games">
  <div class="col-xs-2 col-md-2 thumbnail thumbnail-games" >
    <img src="images/games/lol.png" alt="...">
  </div>
  <div class="col-xs-2 col-md-2 thumbnail thumbnail-games">
    <img src="images/games/overwatch.png" alt="...">
  </div>
  <div class="col-xs-2 col-md-2 thumbnail thumbnail-games">
    <img src="images/games/counterstrike.png" alt="...">
  </div>
  <div class="col-xs-2 col-md-2 thumbnail thumbnail-games">
    <img src="images/games/hearthstone.png" alt="...">
  </div>
  <div class="col-xs-2 col-md-2 thumbnail thumbnail-games">
    <img src="images/games/dota2.png" alt="...">
  </div>
  <div class="col-xs-2 col-md-2 thumbnail thumbnail-games">
    <img src="images/games/sc2.png" alt="...">
  </div>
</div>
</section>

<!--info boxes-->
	<div class="content-section">
			  <div class="container">
				<div class="testi-whole-section">
				<div class="col-md-4">
				  <div class="testimonial-wrapper">
					 <div class="col-content">
						<h5>Expert coaches</h5>
						<p>Learn from some of the most knowledgeable and friendly players in the game.</p>
						<!--<a href="about.php" class="btn btn-secondary content-btn">Our Coaches</a>-->
						<!--<button type="button" class="btn btn-secondary content-btn">Our Coaches</button>-->
					  </div>
				  </div>
				</div>
				<div class="col-md-4">
					<div class="testimonial-wrapper">
					  
					  <div class="col-content">
						<h5>Easy to use</h5>
						<p>No delay, no setup. It's all in browser and totally free.</p>
						<a href="about.php" class="btn btn-secondary content-btn">Try Now</a>
						<!--<button type="button" class="btn btn-secondary content-btn">How IT Works</button>-->
					  </div>
				  </div>
				</div>
				<div class="col-md-4">
					<div class="testimonial-wrapper">
					  
					  <div class="col-content">
						<h5>Get good... fast</h5>
						<p>Stop wasting time with YouTube tutorials, guides, Wikis and forums.</p>
					<!--<a href="https://game.microcoaching.net/Account/SignUp" class="btn btn-secondary content-btn"> Sign Up</a>-->
						<!--<button type="button" class="btn btn-secondary content-btn">Sign Up</button>-->
					  </div>
				  </div>
				</div>
				</div>
			</div>
    </div>  
<!--	
	<div class="container">
		<div class="inner-content center">
			<h2>Coaching Sessions</h2>
			<p>Hundreds of coaching sessions! Check out a few short clips:</p>
		<div class="">	
		<div class="col-md-6 second-player">
			 <video style="padding-top:40px;width:100%;height:326px" id="player2" preload="none">
				  <source src="https://www.youtube.com/watch?v=AkWXfr3AEJg" type="video/youtube">
			 </video>				 
		</div>
		<div class="col-md-6 first-player">
			 <!--<iframe width="517" height="326" src="https://www.youtube.com/watch?v=APnl6XK7L9g"></iframe>
			 <video style="padding-top:40px;width:100%;height:326px" id="player1" preload="none">
				  <source type="video/youtube" src="https://www.youtube.com/watch?v=APnl6XK7L9g" >
			 </video>
		</div>
	
		</div>
		</div>
    </div>				  
<section>
<div  class="container" >
	 <div class="circle-cont" >
		<div class="board-members-block">
		  <div class="col-md-3">
			<div class="testi-img">
			  <img alt="Claire Pieper" class="img-responsive img-circle person_img" src="images/emre_aboutus1.jpg">
			</div>
			
		  </div>
		  <div class="col-md-9">
			<p class="tabbed-content" style="text-align: justify;">
				Obstacles don’t have to stop you. If you run into a wall, don’t turn around and give up. Figure out how to climb it, go through it, or work around it.
				<br><span>&#8211; Emre Ruhi, Coach</span>
			</p>
		  </div>
		  <div style="clear: both;"></div>
		</div>
	 </div>
</div>
</section>
-->
<!--<div class="push"></div>-->
    <?php 

include("footer.php"); 
?>
