(function(){

	var app = angular.module("mc");

	app.service("promoCodesService", function ($http) {

		var updateCode = function(code) {
			var url = "/promo-codes/update/" + code.id;

			return $http({
				url: url,
				params: {
					id: code.id,
                    code: code.code,
                    discount: code.discount
				},
				method: 'POST',
				headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
			});
		};

		var deleteCode = function(code) {

			return $http.delete("/promo-codes/delete/" + code.id);
		};

		return {
			updateCode: updateCode,
			deleteCode: deleteCode
		};
	});
	
}());