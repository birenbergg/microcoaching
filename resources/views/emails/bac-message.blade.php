<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="{{ asset('css/mail.css') }}">
</head>
<body>
    <div id="wrapper">
        <h1>New Coach Application</h1>
        <div id="content">
            <div class="group">
                <label>Name</label>
                <div class="text">{{ $msg->name }}</div>
            </div>

            <div class="group">
                <label>Email</label>
                <div class="text">{{ $msg->email }}</div>
            </div>

            <div class="group">
                <label>Skype / Discord</label>
                <div class="text">{{ $msg->skype_discord }}</div>
            </div>

            <div class="group">
                <label>Phone Number</label>
                <div class="text">{{ ($msg->phone != null) ? $msg->phone : '(not specified)' }}</div>
            </div>

            <div class="group">
                <label>Country / Timezone</label>
                <div class="text">{{ ($msg->country_timezone != null) ? $msg->country_timezone : '(not specified)' }}</div>
            </div>
            
            <div class="group">
                <label>Age</label>
                <div class="text">{{ ($msg->age != null) ? $msg->age : '(not specified)' }}</div>
            </div>
            
            <div class="group">
                <label>Steam Account Name</label>
                <div class="text">{{ $msg->steam }}</div>
            </div>
            
            <div class="group">
                <label>Game(s)</label>
                <div class="text">{{ $msg->game }}</div>
            </div>
            
            <div class="group">
                <label>Gaming Experience</label>
                <div class="text">{{ $msg->gaming_xp }}</div>
            </div>
            
            <div class="group">
                <label>Teaching Experience</label>
                <div class="text">{{ ($msg->teaching_xp != null) ? $msg->teaching_xp : '(not specified)' }}</div>
            </div>
            
            <div class="group">
                <label>OP.GG</label>
                <div class="text">{{ $msg->opgg }}</div>
            </div>
            
            <div class="group">
                <label>Twitter</label>
                <div class="text">{{ ($msg->twitter != null) ? $msg->twitter : '(not specified)' }}</div>
            </div>
            
            <div class="group">
                <label>Twitch</label>
                <div class="text">{{ ($msg->twitch != null) ? $msg->twitch : '(not specified)' }}</div>
            </div>
            
            <div class="group">
                <label>YouTube</label>
                <div class="text">{{ ($msg->youtube != null) ? $msg->youtube : '(not specified)' }}</div>
            </div>
            
            <div class="group">
                <label>Availability</label>
                <div class="text">{{ $msg->availability }}</div>
            </div>
        </div>
    </div>
</body>
</html>
