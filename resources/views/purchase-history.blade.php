@extends('layout.master')
@section('title', 'Your Purchase History')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/dashboard.css') }}">
@stop

@section('content')
    <section>
    	<h2>Your Purchase History</h2>

        @if(count($purchases) > 0)
        	<table>
        		<thead>
        			<tr>
        				<th>Credits</th>
        				<th>Price</th>
        				<th>Date</th>
        			</tr>
        		</thead>
        		<tbody>
        			@foreach ($purchases as $purchase)
        			<tr>
        				<td>{{ $purchase->credits_amount }}</td>
        				<td>${{ $purchase->price }}</td>
        				<td>{{ $purchase->created_at }}</td>
        			</tr>
        			@endforeach
        		</tbody>
        	</table>
        @else
            No purchases yet.
            <br>
            <br>
            <a href="{{ route('packages') }}">Buy credits!</a>
        @endif
    </section>
@stop