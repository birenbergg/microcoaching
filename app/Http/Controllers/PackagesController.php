<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;

use App\Package;
use App\PromoCode;
use App\PaypalExpress;


class PackagesController extends Controller
{
	public function index()
    {
    	$packages = Package::all();
        return view('packages', ['packages' => $packages]);
    }

    public function package($slug)
    {
        $package = Package::where('slug', $slug)->first();

        return view('buy', ['package' => $package, 'promoCode' => null]);
    }

    public function packagePromo($packageSlug)
    {
        $package = Package::where('slug', $packageSlug)->first();
        $promoCode = PromoCode::where('code', Input::get('promocode'))->first();

        return view('buy', ['package' => $package, 'promoCode' => $promoCode]);
    }

    public function paymentProcess($paymentID, $payerID, $token, $packageID, $promoCode = null)
    {
        $paypalExpress = new PaypalExpress();
        $paypalCheck = $paypalExpress->paypalCheck($paymentID, $payerID, $token, $packageID, $promoCode);

        if ($paypalCheck) {
            return redirect()->route('home');
        } else {
            return redirect()->route('packages');
        }
    }
}
