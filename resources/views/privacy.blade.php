@extends('layout.master')
@section('title', 'Privacy Policy')
@section('styles')
    <link rel="stylesheet" href="{{ asset('css/privacy-terms.css') }}">
@stop

@section('content')
    <section>
        <h1>Privacy Policy</h1>

        <h2>Your Privacy Rights</h2>
        <p>Effective Date: April 28, 2018</p>
        <p>This Privacy Policy describes the types of information gathered by Microcoaching Inc. (“Microcoaching”, “us” or “we”) in the process of providing this computing application and the data, services, information, tools, functionality, updates and similar materials provided therethrough (collectively, the “Service”), how we use it, with whom the information may be shared, what choices are available to you regarding collection, use and distribution of information and our efforts to protect the information you provide to us through the Service. By using the Service, you hereby consent to allow us to process information in accordance with this Policy. Please also refer to our <a href="{{ route('terms') }}">Terms of Use</a>, which are incorporated as if fully recited herein.</p>
        <p>This policy is subject to change. If our information retention or usage practices change, we will let you know by posting the Privacy Policy changes on the Service and/or otherwise making you aware of the changes. Please refer to the “Effective Date” above to see when this Policy was last updated.</p>

        <h2>Territoriality</h2>

        <p>Regardless of where our servers are located, your personal data may be processed by us in the United States, where data protection and privacy regulations may or may not be to the same level of protection as in other parts of the world. BY VISITING THE WEBSITE AND USING THE SERVICE, YOU UNEQUIVOCALLY AND UNAMBIGUOUSLY CONSENT TO THE COLLECTION AND PROCESSING IN THE UNITED STATES OF ANY INFORMATION COLLECTED OR OBTAINED BY US THROUGH VOLUNTARY SUBMISSIONS, AND THAT U.S. LAW GOVERNS ANY SUCH COLLECTION AND PROCESSING.</p>

        <h2>Information Collected</h2>

        <p>We collect certain Personal Information about you, which may be supplied when you sign-up for the Service, when you complete a survey, when you create or update an account, when you use the Service, when you request services or otherwise when you submit such information. By way of example and not limitation, the information that may be collected includes:</p>

        <ul>
            <li>Name;</li>
            <li>Address;</li>
            <li>Phone number;</li>
            <li>Email;</li>
            <li>Username;</li>
            <li>Social Media account names and information;</li>
            <li>Information from your activities on the Service; and</li>
            <li>Location data.</li>
        </ul>

        <p>If you log-in to the Service using a social media account, or otherwise connect your social media account with the Service, we may collect additional information about you from those sources, as made available by such social medial platforms.</p>
        <p>We may also collect information about you from third-party sources, including but not limited to gaming platforms, and combine it with data we collect directly from you, including from gaming or other third-party platforms.</p>
        <p>Although the Service may ask for a payment card or other banking information at times to facilitate transactions on the Service, we collect this information through a third-party processor, and we do not take possession of your information.</p>

        <h2>Anonymous Information</h2>
        <p>Anonymous Information is collected about you when you use the Service, including but not limited to links and materials posted, enrollment history, purchase history, the type of device you used and its operating system, the pages accessed most frequently, how pages are used, applications downloaded, search terms entered, and similar data.</p>
        <p>Automatically Tracking Internet Protocol (IP) addresses is one method of automatically collecting information about your activities online and information volunteered by you. An IP address is a number that is automatically assigned to your device whenever you surf the internet. Further, the Service may utilize web beacons, pixel tags, cookies (including third-party cookies), embedded links, and other commonly used information-gathering tools. The Service may also collect device identifiers.</p>
        <p>Although it may be possible to turn off the collection of cookies through your device, that may interfere with your use of the Service. In addition, if you receive targeted advertisements based on your browsing patterns, you may be able to opt-out of such advertisements through links available in such advertisements at that time, such as through an AdChoices icon.</p>
        <p>Part of the purpose of the Service may be to share materials you make available. Anything you publicly post or direct to be made public, such as if you share a session on a public media platform, will not be considered Personal Information, and will be outside the restrictions of this policy.</p>

        <h2>Use of Information</h2>
        <h3>Internal Use of Information</h3>
        <p>We accept and gather Personal Information in an effort to provide the Service to you. We may also use Personal Information to help us develop and improve our Service, fulfill your requests, send materials to you, inform you about our offers and those of others, perform research, tailor our Service to meet your interests, and for other purposes permitted by law.</p>
        <p>We may use Anonymous Information or non-Personal Information for any business purpose.</p>

        <h2>Sharing Information Collected with Third Parties, Consultants, and Affiliates</h2>
        <p>We may share Personal Information with our business associates, consultants, service providers, advisors and affiliates in order for them to provide services to us, to you, and to enable us to provide the Service. For example, our host and internet service provider may have access to this information.</p>
        <p>We may use and share your Personal Information with third-parties for marketing or research purposes, and they may use this information in accordance with their own policies.</p>
        <p>We may incorporate your Personal Information into an aggregate databases, using industry standard methodology to anonymize the data, and we may use such anonymous aggregate databases for our general business purposes, including sharing such databases and any analyses derived therefrom with others.</p>
        <p>Your profile pages may be made public, and as such, any personal information and content you make available on those pages will be publicly available.</p>
        <p>Our Service may use Google Analytics, a web analytics service provided by Google, Inc. (“Google”). Google Analytics uses cookies, to help us analyze how users use the Service. You may be able to opt-out of Google’s collection. In addition, we may use other analytics platforms that may collect similar information during your use of the Service.</p>
        <p>Further, we may disclose collected information to the extent we believe it necessary to comply with the law, such as in response to a subpoena or court order, to defend a legal claim or otherwise as permitted by applicable law. We may disclose any information in our possession in the event that we believe it necessary or appropriate to prevent criminal activity, personal injury, property damage or bodily harm.</p>
        <p>Additionally, we may transfer your information to successor in interest, which may include but may not be limited to a third-party in the event of an acquisition, sale, merger or bankruptcy. Under such a circumstance, this policy may no longer govern your data.</p>

        <h2>Links</h2>
        <p>Our Service may contain links to other websites or services. We are not responsible for the privacy practices of such other sites. When you leave our Service to visit another website or application, please read the privacy statements of websites that may collect personally identifiable information. This Privacy Policy applies solely to information collected by us through the Service.</p>

        <h2>Security</h2>
        <p>We employ procedural and technical safeguards to secure data in our possession, consistent with the sensitivity level of such data. Regardless of the precautions we take, no transmission of data over the internet is guaranteed to be completely secure. It may be possible for third parties not under our control to intercept or access transmissions or private communications unlawfully. While we strive to protect personal information, we cannot ensure or warrant the security of any information you transmit to us.</p>

        <h2>E-Mail and Electronic Newsletters</h2>
        <p>We may offer electronic newsletters and e-mails concerning promotions, new products and services, or other marketing materials as a service to our users. You may receive newsletters and e-mails concerning promotions and marketing of ours, after you register through with the Service. If, after you have received a message, you no longer wish to receive such materials, you may opt-out by following the unsubscribe instructions included in each electronic newsletter and e-mail.</p>

        <h2>Access and Control</h2>
        <p>Some of the information collected about you may be retrievable and changeable through your username login. To do so, follow instructions on the Service.</p>

        <h2>Communication Tools</h2>
        <p>The Service may contain tools that enable you to share personal information, including sensitive information, with others, through social media and in other ways. Should you choose to use such tools, please exercise caution when sharing sensitive information. The Service will not share this information in this manner without you directing it to do so.</p>

        <h2>Do Not Track</h2>
        <p>At this time, the Service does not specifically respond to do-not- track signals.</p>

        <p style="font-weight: bold">We do not knowingly permit users to register for our Service if they are under 13 years old, and therefore do not request personally identifiable information from anyone under the age of 13. If we become aware that a customer is under the age of 13 and has registered without prior verifiable parental consent, we will remove his or her personally identifiable registration information from our files. If you are the parent or guardian of a person under the age of 13 who has provided personally identifiable information to us without your approval, please inform us by contacting us at the e-mail address below and we will remove such information from our database.</p>


        <h2>Contact Information</h2>
        <p>If you have any questions or suggestions regarding our Privacy Policy, please contact us via e-mail at <a href="mailto: support@microcoaching.net">support@microcoaching.net</a>.</p>
    </section>
@stop