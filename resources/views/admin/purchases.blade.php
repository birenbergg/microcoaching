@extends('layout.admin-master')
@section('title', 'User Purchase History')

@section('admin-content')
    <section>
    	<h2>User Purchase History</h2>

        @if(count($purchases) > 0)
        	<table>
        		<thead>
        			<tr>
        				<th>Credits</th>
        				<th>Price</th>
                        <th>Date</th>
                        <th>Buyer's Name</th>
                        <th>Buyer's Email</th>
        			</tr>
        		</thead>
        		<tbody>
        			@foreach ($purchases as $purchase)
        			<tr>
        				<td>{{ $purchase->credits_amount }}</td>
        				<td>${{ $purchase->price }}</td>
                        <td>{{ $purchase->created_at }}</td>
                        <td>{{ $purchase->user->name }}</td>
                        <td>{{ $purchase->user->email }}</td>
        			</tr>
        			@endforeach
        		</tbody>
        	</table>
        @else
            No purchases yet.
        @endif
    </section>
@stop