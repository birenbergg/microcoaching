<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\User;
use App\Package;
use App\PromoCode;
use App\CreditPurchase;

class AdminController extends Controller
{
    // Users

    public function users()
    {
    	$users = User::all();
        return view('admin.users', ['users' => $users]);
    }

    public function updateUser($id)
    {
        $user = User::find($id);

        $user->credits = Input::get('credits');
        
        $user->update();
    }

    // Packages

    public function packages()
    {
    	$packages = Package::all();
        return view('admin.packages', ['packages' => $packages]);
    }

    public function updatePackage($id = 'undefined')
    {
        if ($id == 'undefined') {
            $package = new Package();
        } else {
            $package = Package::find($id);
        }

        $package->name = Input::get('name');
        $package->price = Input::get('price');
        $package->slug = Input::get('slug');
        $package->currency = 'USD';
        $package->credits_amount = Input::get('credits_amount');
        
        if ($id == 'undefined') {
            $package->save();
        } else {
            $package->update();
        }

        return $package;
    }

    public function deletePackage($id)
    {
        $package = Package::find($id);
        $package->delete();
    }

    // Promo Codes

    public function promoCodes()
    {
    	$codes = PromoCode::all();
        return view('admin.promo-codes', ['codes' => $codes]);
    }

    public function updatePromoCode($id = 'undefined')
    {
        if ($id == 'undefined') {
            $code = new PromoCode();
        } else {
            $code = PromoCode::find($id);
        }

        $code->code = Input::get('code');
        $code->discount = Input::get('discount');
        
        if ($id == 'undefined') {
            $code->save();
        } else {
            $code->update();
        }

        return $code;
    }

    public function deletePromoCode($id)
    {
        $promoCode = PromoCode::find($id);
        $promoCode->delete();
    }

    // Purchases

    public function purchases()
    {
    	$purchases = CreditPurchase::with('user')->get();
        return view('admin.purchases', ['purchases' => $purchases]);
    }
}
