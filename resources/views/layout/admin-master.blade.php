@extends('layout.master')

@section('styles')
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">

    <link rel="stylesheet" href="{{ asset('css/dashboard.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin.css') }}">
@stop

@section('content')
    <aside>
        <ul>
            <li class="{{ Route::is('admin.users') ? 'current' : '' }}">
                <a href="{{ route('admin.users') }}"><i class="fas fa-user fa-fw"></i> Manage User Credits</a>
            </li>
            <li class="{{ Route::is('admin.packages') ? 'current' : '' }}">
                <a href="{{ route('admin.packages') }}"><i class="fas fa-cube fa-fw"></i> Manage Packages</a>
            </li>
            <li class="{{ Route::is('admin.promo-codes') ? 'current' : '' }}">
                <a href="{{ route('admin.promo-codes') }}"><i class="fas fa-credit-card fa-fw"></i> Manage Promo Codes</a>
            </li>
            <li class="{{ Route::is('admin.purchases') ? 'current' : '' }}">
                <a href="{{ route('admin.purchases') }}"><i class="fas fa-credit-card fa-fw"></i> User Purchase History</a>
            </li>
        </ul>
    </aside>
    @yield('admin-content')
@stop