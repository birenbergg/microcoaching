<?php 

$title = "chat now! - microcoaching.net - on demand video game coaching";

$metaD = "win more games - video game coaching - get better in league of legends csgo hearthstone";

include("headerchat.php"); 
?>

<!-- <body> -->
    <div class="container">
        <!--<div class="col-md-3">
<img src="images/sidekick3.png" alt="sidekick - powered by microcoaching" class="img-responsive sidekick-logosize center-block" >
            
        </div>-->
        <div class="col-md-9 text-center" style="position:relative; z-index: 999;">

            <h1 style="color: #00a79a;">Your Sidekick is waiting. <span style="color: #428bca;">Totally free!</span></h1>
            <h2 style="margin-top: 0px;">Ask gaming questions below & get expert answers from <a href="/coaches" target="_blank"><u>top coaches</u></a>. EZ.</h2>
            <!--<h3>Coaches online NOW for CS:GO & League of Legends. Give it a try, it's totally free!</h3>-->
            <!-- Add this div in your DOM where you want the inline div to be inserted by Chatlio, change height/width to fit your needs -->
            <div id="chatlioWidgetPlaceholder" style="margin:auto; height: 50vh; "></div>
            <!--div id="chatlioWidgetPlaceholder" style="margin:auto; height:300px; width:350px; "></div>-->

            <!--chatlio script-->
            <script type="text/javascript">
                window._chatlio = window._chatlio || [];
                ! function() {
                    var t = document.getElementById("chatlio-widget-embed");
                    if (t && window.ChatlioReact && _chatlio.init) return void _chatlio.init(t, ChatlioReact);
                    for (var e = function(t) {
                            return function() {
                                _chatlio.push([t].concat(arguments))
                            }
                        }, i = ["configure", "identify", "track", "show", "hide", "isShown", "isOnline"], a = 0; a < i.length; a++) _chatlio[i[a]] || (_chatlio[i[a]] = e(i[a]));
                    var n = document.createElement("script"),
                        c = document.getElementsByTagName("script")[0];
                    n.id = "chatlio-widget-embed", n.src = "https://w.chatlio.com/w.chatlio-widget.js", n.async = !0, n.setAttribute("data-embed-version", "2.1");
                    n.setAttribute('data-widget-options', '{"embedInline": true}');
                    n.setAttribute('data-widget-id', '0dcba86b-31a4-4145-6339-52bbf8172158');
                    c.parentNode.insertBefore(n, c);
                }();

            </script>


            <script type="text/javascript">
                // First we wait for the event to be emitted by the chatlio widget when it is fully rendered on the page
                document.addEventListener('chatlio.ready', function(e) {

                    // We create the dropdown element with jQuery and add it after the email/name field before the chat starts using its unique selector.
                    // The value of the value field in the select element will be rendered inside Slack's individual user support channel

                    $('<select id="chatlio_customdropdown" style="font-size: 18px; max-width: 200px; border-width: 1px"><option value="customer_service">Do you need some Customer Service help?</option><option value="expert_advice">Would you like some advice from an Expert?</option></select>').insertAfter('div.chatlio-widget-body > div > div > input[type="text"]:nth-child(3)');

                    // We grab the value of the selected dropdown option and pass it on to chatlio when the start chat button is pressed
                    // It is important that we populate the `userid` field (shown in the official docs). In this scenario, I opted to go with the generated uuid by chatlio `_chatlio.vsUuid()`

                    $(".chatlio-btn").click(function() {
                        _chatlio.identify(_chatlio.vsUuid(), {
                            dropdown_option: document.getElementById('chatlio_customdropdown').value
                        });
                    });

                }, false);

            </script>

<script type="text/javascript">
            document.addEventListener("chatlio.firstMessageSent", function(event){
analytics.track('Live Chat Conversation Started', {
Category: ‘Coaching Sessions’,
agent_id: '',
agent_name: '',
agent_username: '',
conversation_id: '',
message_body: '',
message_id: '' }); // Do something interesting
  });
  </script>
<!--      <script type="text/javascript">
            analytics.track("chatlio.firstMessageSent", {
  category: 'Chatlio',
  label: 'Firstmsg',
  value: 1
});</script> -->




            <!--end chatlio script-->

            <h4 style="PADDING-TOP: 10PX; PADDING-BOTTOM: 5PX;">
                Sidekick is free, private and secure</h4>
            <a href="https://www.paypal.me/microcoaching/1" style="color:#bdbdbd;" class="center-block">tip your coach!</a>
            <br>

        </div>
        <div class="col-md-3">
            <!--  <h2>Example Questions:</h2>
            <div class="questionbox">
                <h2>"How do I build against a teemo bot lane?" </h2>
            </div>
            <div class="questionbox">
                <h2>"Enemy jungler is Zac - when should I watch out for ganks?" </h2>
            </div>-->

            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <!-- mc test ad -->
            <ins class="adsbygoogle" style="display:block" data-ad-client="ca-pub-9956627773087168" data-ad-slot="8239166115" data-ad-format="auto"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});

            </script>
        </div>
    </div>
    <!--close container-->
    <?php include("footerchat.php"); ?>
