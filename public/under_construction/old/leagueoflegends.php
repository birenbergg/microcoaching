
<?php 

$title = "chat now! - microcoaching.net - on demand video game coaching";

$metaD = "win more games - video game coaching - get better in league of legends csgo hearthstone";

include("headerchat.php"); 
?>

<body>

<div class="container center" style="position:relative; z-index: 999;">
<h2>Get Instant Answers!</h2> 
<h2>Send us a gaming question below and get an expert reply in 90 seconds</h2>
<h3>It's free, private and secure</h3>

<!-- Add this div in your DOM where you want the inline div to be inserted by Chatlio, change height/width to fit your needs -->
<div id="chatlioWidgetPlaceholder" style="margin:auto; height:560px; width:400px; "></div>

<!--chatlio script-->
<script type="text/javascript">
    window._chatlio = window._chatlio||[];
    !function(){ var t=document.getElementById("chatlio-widget-embed");if(t&&window.ChatlioReact&&_chatlio.init)return void _chatlio.init(t,ChatlioReact);for(var e=function(t){return function(){_chatlio.push([t].concat(arguments)) }},i=["configure","identify","track","show","hide","isShown","isOnline"],a=0;a<i.length;a++)_chatlio[i[a]]||(_chatlio[i[a]]=e(i[a]));var n=document.createElement("script"),c=document.getElementsByTagName("script")[0];n.id="chatlio-widget-embed",n.src="https://w.chatlio.com/w.chatlio-widget.js",n.async=!0,n.setAttribute("data-embed-version","2.1");
       n.setAttribute('data-widget-options', '{"embedInline": true}');
       n.setAttribute('data-widget-id','7311895d-b3c8-4f9e-6331-019b3318659f');
       c.parentNode.insertBefore(n,c);
    }();
</script>
<!--end chatlio script-->
</div>

<?php include("footerchat.php"); ?>