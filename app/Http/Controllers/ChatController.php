<?php

namespace App\Http\Controllers;

use Auth;
use App\User;

class ChatController extends Controller
{
	public function text()
    {
        if (Auth::user()->credits > 0) {
            return view('chat-t');
        }
        else {
            return redirect()->route('packages');
        }
    }

    public function video()
    {
        if (Auth::user()->credits > 9) {
            return view('chat-v');
        }
        else {
            return redirect()->route('packages');
        }
    }

    public function oneCreditDown($id)
    {
        $user = User::find($id);

        $user->credits--;
        
        $user->update();
    }
}
