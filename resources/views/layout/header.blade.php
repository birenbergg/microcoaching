<header>
    <div id="header-container">
        <a href="{{ route('home') }}" id="logo">
            <img src="{{ asset('images/logo.png') }}" alt="Logo">
        </a>
        <nav>
            <a class="{{ Route::is('bac') ? 'active' : '' }}" href="{{ route('bac') }}">Become A Coach</a>
            <a class="{{ Route::is('packages') ? 'active' : '' }}" href="{{ route('packages') }}">Packages</a>
            <a class="{{ Route::is('about') ? 'active' : '' }}" href="{{ route('about') }}">About</a>
            <a class="{{ Route::is('contact') ? 'active' : '' }}" href="{{ route('contact') }}">Contact</a>
            @guest
                <a href="{{ route('login') }}">Log In</a>
            @else
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre
                         onclick="event.preventDefault(); document.getElementsByClassName('dropdown-menu')[0].style.display == 'block' ? document.getElementsByClassName('dropdown-menu')[0].style.display = 'none' : document.getElementsByClassName('dropdown-menu')[0].style.display = 'block';">

                        <img class="user" src="//www.gravatar.com/avatar/{{ md5(strtolower(trim(Auth::user()->email))) }}?d=mm">
                    </a>

                    <ul class="dropdown-menu">
                        <li id="user-info">
                            <div id="user-name">
                                {{ Auth::user()->name }}
                            </div>

                            @if (!Auth::user()->is_admin)
                            <div id="user-credits" class="{{ Auth::user()->credits > 0 ? 'has-credits' : '' }}">
                                Credits: {{ Auth::user()->credits }}
                            </div>
                            @endif
                        </li>

                        @if (Auth::user()->is_admin)
                        <li>
                            <a href="{{ route('dashboard') }}">
                                <i class="fas fa-sliders-h fa-fw"></i>
                                Admin Dashboard
                            </a>
                        </li>
                        @endif
                        <li>
                            <a href="{{ route('purchase-history') }}">
                                <i class="fas fa-credit-card fa-fw"></i>
                                Purchase History
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="fas fa-sign-out-alt fa-fw"></i>
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
            @endguest
            <a class="cta" href="{{ route('router') }}">Start</a>
        </nav>
        <img id="show-menu" src="{{ asset('images/icons/show-menu.svg') }}" alt="" onclick="document.getElementsByTagName('header')[0].className += ' mobile';">
        <img id="hide-menu" src="{{ asset('images/icons/hide-menu.svg') }}" alt="" onclick="document.getElementsByTagName('header')[0].className = document.getElementsByTagName('header')[0].className.replace(/\bmobile\b/,'');">
    </div>
</header>