(function(){

	var app = angular.module("mc");

	app.service("chatService", function ($http) {

		var oneCreditDown = function(id) {
			var url = "/chat/onecreditdown/" + id;

			return $http({
				url: url,
				params: {
                    id: id
				},
				method: 'POST',
				headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
			});
		};

		return {
			oneCreditDown: oneCreditDown
		};
	});
	
}());