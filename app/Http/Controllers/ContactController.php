<?php

namespace App\Http\Controllers;

use Mail;
use Illuminate\Http\Request;

class ContactController extends Controller
{
	public function get()
    {
        return view('contact');
    }

    public function post(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required',
        ]);

        Mail::send('emails.contact-message', [
            'msg' => $request
        ], function($mail) use($request) {
            $mail->from('no-reply@microcoaching.net', 'Microcoaching Contact Form');
            $mail->to('microcoachinginc@gmail.com')->subject('New Contact Message');
        });

        return redirect()->route('home');
    }
}
