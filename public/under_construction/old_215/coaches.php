<?php 
$title = "chat now! - microcoaching.net - on demand video game coaching";

$metaD = "win more games - video game coaching - get better in league of legends csgo hearthstone";

include("header.php"); 
?>

<!-- <body> -->

<div class="container">
  <section>
    <div class="with-us">
<p>Our team of select coaches are highly ranked in their respective games <i>and</i> they're excellent teachers.
<br><br>
We make sure every coach is proven effective, and that's why competitive gamers love Microcoaching.</p>
  </section>
</div>
<div class="with-us">
      <h1 class="text-center">Featured Coaches</h1>
</div>


<div class="container">
<div class="row">
  
  <div class="col-sm-6 col-md-6">
    <div class="thumbnail" style="padding-top: 15px;">
      <img src="/images/jerry.png" alt="..." style="max-height: 160px;" class="img-circle">
      <div class="caption">
        <h2 class="text-center">Jerry Yang</h2>
		<p>Jerry has had three years of DOTA experience before moving to League of Legends in 2010, placing mid diamond in every season since S3. As a fill player and coach of the UMD uLoL team in the 2017 season, he has knowledge of a wide variety of roles and macro gameplay. He believes that a positive attitude and smart decision making wins games rather than raw mechanical skill.</p>
        <p><a href="/sidekick" class="btn btn-secondary ourtools center-block" role="button">Ask Jerry Now!</a></p>
      </div>
    </div>
  </div>
  
    <div class="col-sm-6 col-md-6">
    <div  class="thumbnail" style="padding-top: 15px;">
      <img src="/images/larry.png" alt="..." style="max-height: 160px;" class="img-circle">
      <div class="caption">
        <h2 class="text-center">Larry Zhang</h2>
	<p>Larry used to be Bronze in Season 1, Gold 1 in S2 and then ended up Challenger just four days into preseason 3. He started coaching in Season 3.. As a coach, he hopes to allow players to flourish on whatever champion it is that you want to main and any role. Larry will teach you how you can learn and grow with every game, and how to play in a way that is more consistent. He believes that learning to play smart over brute forcing wins.</p>
  	<p><a href="/sidekick" class="btn btn-secondary ourtools center-block" role="button">Talk to Larry Now!</a></p>

       </div>
    </div>
  </div>
  
  
    <!-- <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="/images/mph.png" alt="...">
      <div class="caption">
        <h2>Mythic Plus Helper - MPH</h2>
        <p>Mythic Plus Helper is a tool that allows you to vet your Mythic+ applicants and group members quickly and efficiently. Get all the most important info about every player with a single keystroke using our Official MPH Addon.
 </p>
		<p><a href="http://mythicplus.help/" class="btn btn-secondary ourtools center-block" role="button">Get MPH</a></p>
       </div>
    </div>
  </div>
  -->

</div><!--row close-->
</div><!--container close-->
<section class="bottom-cnt" style="padding-bottom: 160px;">
<div class="container">
  <section>
    <div class="with-us">
      <h1 class="text-center">Our Coach's Mission</h1>
      <p>I sent a friend invite to invertedcomposer, the Season 2 best Singed in NA at the time, widely accredited as so by Dyrus. He was willing to accept my invite, and even teach me how to play Singed with his informative yet short videos. That alone was the step I needed to awaken myself and to reach Challenger. He helped me get from Gold 5 to Gold 1 in S2, and then in S3, I was able to practice vs popular pros like RFlegendary, voyboy, etc. top lane, who all destroyed me by the way, and I was able to use those match ups into learning experiences. 
<br><br>
I believe it is my job to give back, just like how invertedcomposer was willing to do for me, and to also use my chances of facing the best top laners in the world and turn them into credible insight for the rest of you who are not as fortunate to face them. - Larry Z, 2017
</p>

<p>Think you have what it takes to be a coach? <a style="color: blue" href="/becomeacoach">Apply Here</a></p> 
  </section>
</div>
</section>


<?php include("footer.php"); ?>



