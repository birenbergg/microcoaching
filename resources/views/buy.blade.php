@extends('layout.master')

@section('title')
    {{ $package->name }} Package
@stop

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/packages.css') }}">
@stop

@php
    $newPrice = $promoCode != null ? $package->price - $package->price / 100 * $promoCode->discount : $package->price
@endphp

@section('scripts')
	<script src="//code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="{{ asset('js/testimonials.js') }}"></script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-115361507-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-115361507-1');
	</script>
    <script src="//www.paypalobjects.com/api/checkout.js"></script>
    <script>
        paypal.Button.render({
            env: 'production',
            client: {
                production: "{{ env('PAYPAL_CLIENT_ID') }}"
            },

            // Show the buyer a 'Pay Now' button in the checkout flow
            commit: true,

            // payment() is called when the button is clicked
            payment: function(data, actions) {

                // Make a call to the REST api to create the payment
                return actions.payment.create({
                    payment: {
                        transactions: [
                        {
                            amount: {
                                total: '{{ $newPrice }}',
                                currency: '{{ $package->currency }}'
                            }
                        }]
                    }
                });
            },

            // onAuthorize() is called when the buyer approves the payment
            onAuthorize: function(data, actions) {
                // Make a call to the REST api to execute the payment
                return actions.payment.execute().then(function() {
                    window.location = "{{ env('APP_URL') }}"+"/payment-process/"+data.paymentID+"/"+data.payerID+"/"+data.paymentToken+"/{{ $package->id }}/{{ $promoCode != null ? $promoCode->code : '' }}";
                });
            }
        }, '#paypal-button-container');
    </script>
@stop

@section('content')
    <section>
        <div style="width: 100%; max-width: 800px; padding: 25px; background: #fff; box-sizing: border-box; display: flex; flex-direction: column; box-shadow: 0 4px 15px rgba(0, 0, 0, .5);">

            <div style="display: flex;">
                <div style="flex-grow: 3; display: flex; flex-direction: column; @if($promoCode != null) align-items: center; @endif">
                    <h4 style="font-size: 24px; margin: 0">Buy {{ $package->name }} Package</h4>
                    <p class="credits">{{ $package->credits_amount }} credits</p>
                    <p class="price" style="font-size: 24px; margin-top: 0">
                        Your subtotal:
                        @if($promoCode == null)
                            ${{ $package->price }}
                        @else
                            ${{ $newPrice }}
                        @endif
                    </p>
                    @if($promoCode != null)
                        <label>
                            Promo Code: {{ $promoCode->code }} ({{ $promoCode->discount }}% discount)
                        </label>
                    @endif
					
                </div>

                @if($promoCode == null)
                    <form method="get" action="{{ route('packagePromo', [$package->slug]) }}" style="padding: 0">
                        {{ csrf_field() }}                            
                        <h4 style="font-size: 18px; margin: 0">
                            Promo Code
                        </h4>
                        <div style="display: flex; align-items: center;">
                            <input type="text" name="promocode">
                            <button>Apply</button>
                        </div>
                    </form>
                @endif
            </div>

            <div style="padding: 1em 0; text-align: center;">
                <hr style="margin: 2em 0;">
                <div id="paypal-button-container"></div>
            </div>
			<h4 style="font-size: 12px; margin: 0">
                * Warning &ndash; currently coaching services only for 'League of Legends'.<br>
                <br>
                We plan to add more games soon but if a question for any other game then LoL then please contact our support team and do not purchase credits.
            </h4>
        </div>
    </section>
@stop