(function(){

	var app = angular.module("mc");

	app.service("packagesService", function ($http) {

		var updatePackage = function(package) {
			var url = "/packages/update/" + package.id;

			return $http({
				url: url,
				params: {
					id: package.id,
					name: package.name,
					price: package.price,
					slug: package.slug,
					credits_amount: package.credits_amount,
				},
				method: 'POST',
				headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
			});
		};

		var deletePackage = function(package) {

			return $http({
				url: "/packages/delete/" + package.id,
				method: 'GET'
			});
		};

		return {
			updatePackage: updatePackage,
			deletePackage: deletePackage
		};
	});
	
}());