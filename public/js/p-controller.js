(function(){

	angular.module("mc").controller("packagesController", ['$scope', 'packagesService', '$filter', function ($scope, packagesService, $filter) {

		$scope.selectedPackage = {};

		$scope.packages = [];

		$scope.showModal = function(package) {

			$scope.selectedPackage = {};

			if (!angular.equals(package, {})) { // if package exists
				$scope.selectedPackage.id = package.id;
				$scope.selectedPackage.name = package.name;
				$scope.selectedPackage.price = package.price;
				$scope.selectedPackage.credits_amount = package.credits_amount;
				$scope.selectedPackage.slug = package.slug;
			}
			document.getElementById('modal').classList.add('modal-open');
		}

		$scope.updatePackage = function() {
			packagesService.updatePackage($scope.selectedPackage).then(function(data) {

				var package = $filter('filter')($scope.packages, {id: data.data.id}, true)[0];
				
				if (package === undefined) {
					$scope.packages.push(data.data);
				} else {
					package.id = data.data.id;
					package.name = data.data.name;
					package.credits_amount = data.data.credits_amount;
					package.price = data.data.price;
					package.slug = data.data.slug;
					package.currency = data.data.currency;
				}

				document.getElementById('modal').classList.remove('modal-open');
			});
		}

		$scope.deletePackage = function(package) {
			if(confirm("Are you sure?"))
			{
				packagesService.deletePackage(package).then(function() {
					var index = $scope.packages.indexOf(package);
					$scope.packages.splice(index, 1);
				});

			}
		}

		// When Escape pressed, close modal
		$(document).keypress(function(event) {
			if (event.keyCode === 27) {// esc
				$scope.hideModal();
			}
		});

		// Same as above, Chrome wrokaround
		$(document).keydown(function(event) {
			if (event.keyCode === 27) {// esc
				$scope.hideModal();
			}
		});

		$scope.hideModal = function() {
			document.getElementById('modal').classList.remove('modal-open');
		}
	}]);

}());