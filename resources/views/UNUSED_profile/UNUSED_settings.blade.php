@extends('layout.profile-master')
@section('title', 'Profile Settings')

@section('profile-content')
    <section>
    	<h2>Profile Settings</h2>

    	<div>
    		<form>
    			<div>
	    			<label>Name</label>
	    			<input type="text" name="name" value="{{ Auth::user()->name }}">
    			</div>
    			<div>
	    			<label>Email</label>
	    			<input type="email" name="email" value="{{ Auth::user()->email }}">
    			</div>
    			<div>
	    			<label>
	    				<input type="checkbox" name="change-password">
	    				Change Password
	    			</label>
    			</div>
    			<div>
	    			<label>Password</label>
	    			<input type="password" name="password">
    			</div>
    			<div>
	    			<label>Confirm Password</label>
	    			<input type="password" name="confirm-password">
    			</div>
    			<div>
    				<input type="submit" value="Save">
    			</div>
    		</form>
    	</div>
    </section>
@stop