(function(){

	var app = angular.module("mc");

	app.service("usersService", function ($http) {

		var updateUser = function(user) {
			var url = "/users/update/" + user.id;

			console.log('ID: ' + user.id);
			console.log('Credits: ' + user.credits);

			return $http({
				url: url,
				params: {
					id: user.id,
					credits: user.credits,
				},
				method: 'POST',
				headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
			});
		};

		return {
			updateUser: updateUser
		};
	});
	
}());