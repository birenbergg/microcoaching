<?php 
$title = "chat now! - microcoaching.net - on demand video game coaching";

$metaD = "win more games - video game coaching - get better in league of legends csgo hearthstone";

include("header.php"); 
?>

<!-- <body> -->


<div class="with-us">
      <h1 class="text-center">our featured coaches</h1>
</div>


<div class="container">
<div class="row">
  
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail" style="padding-top: 15px;"">
      <img src="/images/jerry.png" alt="..." style="max-height: 160px;" class="img-circle">
      <div class="caption">
        <h2 class="text-center">Jerry Yang</h2>
        <p>Official coach for the University of Maryland Collegiate League of Legends team.</p>
		<p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

</p>
        <p><a href="/sidekick.php" class="btn btn-secondary ourtools center-block" role="button">Ask Jerry Now!</a></p>
      </div>
    </div>
  </div>
      <div class="col-sm-6 col-md-4">
    <div class="thumbnail" style="padding-top: 15px;"">
      <img src="/images/matthew.png" alt="..." style="max-height: 160px;" class="img-circle">
      <div class="caption">
         <h2 class="text-center">Matthew Sykes</h2>
        <p>Master rank in League and UMD Division I CSL Team member</p>
   <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
</p>
    <p><a href="/sidekick.php" class="btn btn-secondary ourtools center-block" role="button">Chat with Matthew!</a></p>
       
       </div>
    </div>
  </div>
  
    <div class="col-sm-6 col-md-4">
    <div  class="thumbnail" style="padding-top: 15px;"">
      <img src="/images/larry.png" alt="..." style="max-height: 160px;" class="img-circle">
      <div class="caption">
        <h2 class="text-center">Larry Zhang</h2>
        <p>Challenger level League player and one of our top rated coaches.</p>
	<p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

</p>
  	<p><a href="/sidekick.php" class="btn btn-secondary ourtools center-block" role="button">Talk to Larry Now!</a></p>

       </div>
    </div>
  </div>
  
  
    <!-- <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="/images/mph.png" alt="...">
      <div class="caption">
        <h2>Mythic Plus Helper - MPH</h2>
        <p>Mythic Plus Helper is a tool that allows you to vet your Mythic+ applicants and group members quickly and efficiently. Get all the most important info about every player with a single keystroke using our Official MPH Addon.
 </p>
		<p><a href="http://mythicplus.help/" class="btn btn-secondary ourtools center-block" role="button">Get MPH</a></p>
       </div>
    </div>
  </div>
  -->

</div><!--row close-->
</div><!--container close-->
<section class="bottom-cnt" style="padding-bottom: 160px;">
<div class="container">
  <section>
    <div class="with-us">
      <h1 class="text-center">about our coaches</h1>
      <p>Our team of select coaches are highly ranked in their respective games and they're excellent teachers.</p>
<p>We make sure every coach is proven effective, and that's why gamers keep asking for more sessions. </p>
<p>Think you have what it takes to be a coach? <a style="color: blue" href="/becomeacoach.php">Apply Here</a></p> 
  </section>
</div>
</section>


<?php include("footer.php"); ?>



