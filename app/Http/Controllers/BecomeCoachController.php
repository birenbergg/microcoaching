<?php

namespace App\Http\Controllers;

use Mail;
use Illuminate\Http\Request;

class BecomeCoachController extends Controller
{
	public function get()
    {
        return view('bac');
    }

    public function post(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'skype_discord' => 'required|email',
            'country_timezone' => 'required',
            'steam' => 'required',
            'game' => 'required',
            'gaming_xp' => 'required',
            'opgg' => 'required',
            'availability' => 'required'
        ]);

        Mail::send('emails.bac-message', [
            'msg' => $request
        ], function($mail) use($request) {
            $mail->from('no-reply@microcoaching.net', 'Coach Application Form');
            $mail->to('microcoachinginc@gmail.com')->subject('New Coach Application');
        });

        return redirect()->route('home');
    }
}
