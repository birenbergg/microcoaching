@extends('layout.admin-master')
@section('title', 'Packages')

@section('styles')
    @parent <link rel="stylesheet" type="text/css" href="/css/modal.css">
@stop

@section('scripts')
    <script src="//code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular-cookies.js"></script>
    
    <script src="/js/app.js"></script>
    <script src="/js/p-service.js"></script>
    <script src="/js/p-controller.js"></script>
@stop

@section('ng-controller', 'packages')

@section('admin-content')
    <section ng-init="packages = {{ $packages }}">
        <h2>Packages</h2>

        <table ng-show="packages.length > 0">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Credits</th>
                    <th>Slug</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr ng-repeat="package in packages">
                    <td>@{{ package.name }}</td>
                    <td>@{{ package.price }}</td>
                    <td>@{{ package.credits_amount }}</td>
                    <td>@{{ package.slug }}</td>
                    <td style="text-align: right;">
                        <button ng-click="showModal(package)">Edit</button>
                        <button style="background: red" ng-click="deletePackage(package)">Delete</button>
                    </td>
                </tr>
            </tbody>
        </table>

        <button ng-click="showModal({})">Add</button>

        <!-- Modal -->
        <div id="modal" class="modal-wrapper">
            <div class="modal">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-title">@{{ selectedPackage.id == undefined ? 'Add' : 'Edit' }} Package</h2>
                    </div>
                    <div class="modal-body">
                        <div>
                            <div class="label">Name:</div>
                            <div class="input-wrapper"><input id="name" ng-model="selectedPackage.name" value="@{{ selectedPackage.name }}" /></div>
                        </div>
                        <div>
                            <div class="label">Price:</div>
                            <div class="input-wrapper"><input id="credits" ng-model="selectedPackage.price" value="@{{ selectedPackage.price }}" /></div>
                        </div>
                        <div>
                            <div class="label">Credits:</div>
                            <div class="input-wrapper"><input id="credits" ng-model="selectedPackage.credits_amount" value="@{{ selectedPackage.credits_amount }}" /></div>
                        </div>
                        <div>
                            <div class="label">Slug:</div>
                            <div class="input-wrapper"><input id="credits" ng-model="selectedPackage.slug" value="@{{ selectedPackage.slug }}" /></div>
                        </div>
                    </div>
                    <div id="modal-buttons-wrapper">
                        <button type="button" ng-click="updatePackage()" class="normal">Save</button>
                        <button type="button" ng-click="hideModal()" class="cancel">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop