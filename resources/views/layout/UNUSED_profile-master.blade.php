@extends('layout.master')

@section('styles')
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">

    <link rel="stylesheet" href="{{ asset('css/dashboard.css') }}">
@stop

@section('content')
    <aside>
        <ul>
            <li class="{{ Route::is('purchase-history') ? 'current' : '' }}">
                <a href="{{ route('purchase-history') }}">
                    <i class="fas fa-credit-card fa-fw"></i>
                    Your Purchase History
                </a>
            </li>
        </ul>
    </aside>
    @yield('profile-content')
@stop