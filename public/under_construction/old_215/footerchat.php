</body>
<footer class="footer">
<section class="footer-up">
  <div class="container" >
    <div class="try-section">Worth it, right? <div class="executive-box">
    <!--<button class="btn btn-secondary sign-up" type="button">SIGN UP NOW!</button>-->
    </div></div>
  </div>
  </section>



<!-- Begin MailChimp Signup Form -->
<link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet" type="text/css">
<style type="text/css">
	#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; width:100%; display:inline;}
	/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
	   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
</style>
<div id="mc_embed_signup" style="display:inline;">
<form action="//microcoaching.us13.list-manage.com/subscribe/post?u=4f5b24e3f801d6d00d0d5814b&amp;id=fcc3d87e8e" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll" style="display:inline;">
	<label for="mce-EMAIL" style="display:inline;">Stay Updated!</label>
	<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_4f5b24e3f801d6d00d0d5814b_fcc3d87e8e" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Let's Go!" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
    </div>
</form>
</div>
<!--End mc_embed_signup-->
<div class="footer-down foot-bg">
		<div class="col-md-12">
           <p class="footer-text"><a href="http://www.twitter.com/microcoaching_">twitter</a> - <a href="http://www.facebook.com/microcoach">facebook</a> - <a href="mailto:coach@microcoaching.net">email</a></p>
           <p class="footer-text">copyright 2017 - <a href="http://www.microcoaching.net">microcoaching.net</a> - <a href="privacy">privacy policy</a> - patent pending</p>

		   </div>
  </div>

  <script>
$(".help").click(function() {
$(".fadebg").fadeToggle();
});
</script>

<script>
$(".fadebg").click(function() {
$(".fadebg").hide();
});
</script>
      </footer>


    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jssor.slider.mini.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/owl.carousel.js"></script>
	<script src="js/player.js"></script>
	<link  href="css/player.css" rel="Stylesheet" />
	
</html>