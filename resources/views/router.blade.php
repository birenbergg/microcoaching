@extends('layout.master')
@section('title', 'Router')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/dashboard.css') }}">
    <link rel="stylesheet" href="{{ asset('css/router.css') }}">
    <link rel="stylesheet" type="text/css" href="/css/modal.css">
@stop

@section('scripts')
    <script src="//code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="{{ asset('js/testimonials.js') }}"></script>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-115361507-1"></script>
	<script>
        window.dataLayer = window.dataLayer || [];
        function gtag() { dataLayer.push(arguments); }
        gtag('js', new Date());
        gtag('config', 'UA-115361507-1');
	</script>

    <script src="//code.jquery.com/jquery-3.3.1.min.js"></script>
    <script>
        $('#text-chat-button').click(function(event) {
            if ({{ Auth::user()->credits }} == 0) {
                event.preventDefault();
                $('#modal').css('visibility', 'visible');
            }
        });

        $('#video-chat-button').click(function(event) {
            if ({{ Auth::user()->credits }} < 10) {
                event.preventDefault();
                $('#modal').css('visibility', 'visible');
            }
        });

        $('#close').click(function() {
            event.preventDefault();
            $('#modal').css('visibility', 'hidden');
        });
    </script>

@stop

@section('content')
    <section id="router-section">
        <div class="action">
            <div>
                <img src="{{ asset('images/chat.svg') }}" alt="Start to Chat">
                <a id="text-chat-button" href="{{ route('chat-text') }}">Instant Chat</a>
            </div>
            <div class="instructions">
                <p>Just got a quick question? Our expert League coaches are here to help. Feel free to chat as long as you want.</p>
                <p class="bottom-line">10 mins equals one credit.</p>
            </div>
        </div>
        <div class="action">
            <div>
                <img src="{{ asset('images/video-chat.svg') }}" alt="Start Up A Video Chat">
                <a id="video-chat-button" href="{{ route('chat-video') }}">Video Session</a>
            </div>
            <div class="instructions">
                <p>Need deeper insights? Come try a live personalized video coaching session with seamless in-browser screen-sharing. Get better, faster.</p>
                <p class="bottom-line">60 mins equals 10 credits.</p>
            </div>
        </div>
        <div class="action">
            <div>
                <img src="{{ asset('images/credits.svg') }}" alt="Buy More Credits">
                <a href="{{ route('packages') }}">More Credits</a>
            </div>
            <div class="instructions">
                <p>Running low on credits? Get more credits with our flexible plans and get back to chatting with top league coaches right away.</p>
            </div>
        </div>

        <!-- Modal -->
        <div id="modal" class="modal-wrapper">
            <div class="modal">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-title">Please buy more credits</h2>
                    </div>
                    <div class="modal-body" style="padding-bottom: 30px">
                        <div style="flex-direction: column; align-items: center; text-align: center;">
                            <h3 style="margin: 0">Oops!</h3>
                            <p>
                                Looks like you ran out of credits.<br>
                                Buy more and get back to chatting with your coach.
                            </p>
                        </div>
                    </div>
                    <div id="modal-buttons-wrapper" style="flex-direction: column;">
                        <a href="/packages" class="button" style="margin-bottom: 1em;">Buy now!</a>
                        <a href="#" id="close" class="normal">Buy later</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop