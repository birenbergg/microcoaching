@extends('layout.master')
@section('title', 'Home')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/home.css') }}">
@stop

@section('scripts')
    <script src="//code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="{{ asset('js/testimonials.js') }}"></script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-115361507-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-115361507-1');
	</script>

@stop

@section('content')
    <section id="slider">
        <div id="message">
            <h3>
                <!-- <u>FREE</u> Advice from Expert League Coaches -->
                Expert advice from top league coaches
            </h3>
            <p>Get personalized help from an expert gamer NOW and win more competitive games</p>
			<h3>Try It Now for <u>FREE</u>!</h3>
            <a class="cta" href="router">Start</a>
        </div>
        <!-- <div id="scroll-down">
            <div class="mouse">
                <div class="scroller"></div>
            </div>
        </div> -->
    </section>

    <!-- Features -->
    <section id="features" class="features">
        <div class="feature">
            <a class="feature-container" href="//www.youtube.com/channel/UCgjlaTBEmijBWke8VAs__dg" target="_blank">
                <img src="{{ asset('images/tools-content.svg') }}" alt="Tools & Content">
                <h4>Tools & Content</h4>
                <div>Our latest original content and innovative tools curated to help improve your gaming skills at your own pace. FREE!</div>
            </a>
        </div>

        <div class="feature">
            <a class="feature-container" href="router">
                <img src="{{ asset('images/instant-chat.svg') }}" alt="Instant Chat">
                <h4>Instant Chat</h4>
                <div>On-demand expert advice from top coaches – <!--24/7-->from any device! Stop gaming alone, our coaches are LIVE NOW!</div>
            </a>
        </div>
        
        <div class="feature">
            <a class="feature-container" href="router">
                <img src="{{ asset('images/video-coaching.svg') }}" alt="Video Coaching">
                <h4>Video Coaching</h4>
                <div>Face-to-Face coaching with screen-sharing to get deeper insights personalized to your gaming style. GET BETTER, FASTER!</div>
            </a>
        </div>
    </section>

    <!-- Testimonials -->
    <section id="testimonials">
        <div id="testimonial-container">
            <p id="testimonial">"This is a testimonials. We loved the service so much so we wrote a testimonials"</p>
        </div>
    </section>

    <!-- Features -->
    <section id="other-features" class="features">
        <div class="feature">
            <span class="feature-container">
                <img src="{{ asset('images/live-expert-coaches.svg') }}" alt="">
                <h4>Live Expert Coaches</h4>
                <div>Learn from the most knowledgeable and friendly coaches in the game.</div>
            </span>
        </div>
        <div class="feature">
            <span class="feature-container">
                <img src="{{ asset('images/easy-to-use.svg') }}" alt="">
                <h4>Easy to Use</h4>
                <div>Legit info and immediately connect you to an expert gamer. EZ</div>
            </span>
        </div>
        <div class="feature">
            <span class="feature-container">
                <img src="{{ asset('images/get-good-fast.svg') }}" alt="">
                <h4>Get Good Fast</h4>
                <div>Stop wasting time searching online for out-of-date and conflicting tips.</div>
            </span>
        </div>
    </section>

    <section id="logos">
        <img class="unavailable" src="{{ asset('images/games/hearthstone.png') }}" alt="HearthStone">
        <img class="unavailable" src="{{ asset('images/games/counterstrike.png') }}" alt="Counter Strike">
        <img class="available" src="{{ asset('images/games/lol.png') }}" alt="League of Legends">
        <img class="unavailable" src="{{ asset('images/games/dota2.png') }}" alt="Dota 2">
        <img class="unavailable" src="{{ asset('images/games/overwatch.png') }}" alt="Overwatch">
    </section>
@stop