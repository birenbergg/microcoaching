</body>
<footer class="footer">
<section class="footer-up">
  <div class="container" >
    <div class="try-section">Worth it, right? <div class="executive-box">
    <!--<button class="btn btn-secondary sign-up" type="button">SIGN UP NOW!</button>-->
    </div></div>
  </div>
  </section>


<div class="footer-down foot-bg">
		<div class="col-md-12">
           <p class="footer-text"><a href="http://www.twitter.com/microcoaching_">twitter</a> - <a href="http://www.facebook.com/microcoach">facebook</a> - <a href="mailto:coach@microcoaching.net">email</a></p>
           <p class="footer-text">copyright 2017 - <a href="http://www.microcoaching.net">microcoaching.net</a> - <a href="/privacy">privacy policy</a> - patent pending</p>

		   </div>
  </div>

  <script>
$(".help").click(function() {
$(".fadebg").fadeToggle();
});
</script>

<script>
$(".fadebg").click(function() {
$(".fadebg").hide();
});
</script>
      </footer>


    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jssor.slider.mini.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/owl.carousel.js"></script>
	<script src="js/player.js"></script>
	<link  href="css/player.css" rel="Stylesheet" />
	

</html>