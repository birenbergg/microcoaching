<?php 

$title = "about us - microcoaching.net - on demand video game coaching";

$metaD = "win more games - video game coaching - get better in league of legends csgo hearthstone";

include("header.php"); 
?>
<!-- <body> -->

	<div class="bag-img">
	<div class="banner-section container">
		<h1 class="text-center">About Us & FAQ</h1>
	</div>
	</div>
	<div class="container">
	<!-- Begin MailChimp Signup Form -->
<link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet" type="text/css">
<style type="text/css">
	#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; width:100%; display:inline;}
	/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
	   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
</style>
<div id="mc_embed_signup" style="display:inline;">
<form action="//microcoaching.us13.list-manage.com/subscribe/post?u=4f5b24e3f801d6d00d0d5814b&amp;id=fcc3d87e8e" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll" style="display:inline;">
	<label for="mce-EMAIL" style="display:inline;">Stay Updated!</label>
	<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_4f5b24e3f801d6d00d0d5814b_fcc3d87e8e" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Let's Go!" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
    </div>
</form>
</div>
<!--End mc_embed_signup-->
	
	</div>
<div class="container">
	<div class="about-cnt">
		<p>Microcoaching is for any gamer who wants to rank up, improve, or just win more competitive games. </p>
		<span>We know how much it sucks to lose and how hard it can be to improve. Microcoaching gives you legit info by connecting you to an expert gamer, on-demand, for free.</span>
		<span>Don't waste time googling for your specific question and getting bogged down by tons of conflicting answers. Just <a href="/sidekick"><u>connect to a microcoach</u></a>, then get back to having fun. </span>
	</div>
</div>
	<section class="row col-user aboutus">
		<div class="container">
		<div class="col-md-8 col-md-offset-2">
			<h1 class="text-center" style="color:#00b5ac;">our story</h1>
		<div class="text-center">
		<p>Microcoaching was founded in Spring 2016 by Georgetown University MBA students, Emre and Dan.</p>
		<p>They've been playing games competitively and casually for decades and their mission is to make it easier for gamers to improve.</p>
		</div>
			<div class="col-md-6 about-testi">
				<img src="images/emre_aboutus1.jpg" class="img-circle" width="250px">
				<h1>emre ruhi</h1>
				<p><b>Team Fortress:</b> Demoman extraordinare; <b>Starcraft BW:</b> BGH No Rush 20 Expert</p>
			</div>
			<!--<div class="col-md-4 about-testi">
				<img src="images/dwight_aboutus2.jpg" class="img-circle" width="250px">
				<h1>dwight stalls</h1>
				<p><b>The Sims 3 Pets:</b> Competitive equestrian; <b>MLB 2K16:</b> Furious couch casual</p>
			</div>-->
			<div class="col-md-6 about-testi">
				<img src="images/dan_aboutus3.jpg" class="img-circle" height="243px" width="243px">
				<h1>dan tasch</h1>
				<p><b>Law Simulator 2016:</b> Level III Attorney; <b>Fifa:</b> Dorm Champ</p>
			</div>
		</div>
		</div>
	</section>




<section class="row col-user faq">
		<div class="container">
		<div class="col-md-8 col-md-offset-2">
			<h1 class="text-center" style="color:#00b5ac;">FAQ</h1>
	
    <div class="faq-cnt">
        <p>What is Microcoaching?</p>
        <span style="margin-bottom:30px; margin-top:-5px">On-demand coaching for video game players. If you're stuck in a game, want to improve or want to win more, sign up and get a real coach, right away.</span>
<br><br>
        <p>How does it work?</p>
        <span style="margin-bottom:30px; margin-top:-5px">It's easy, just click "try now" up top and start your session - it's free!</span>
<br><br>
        <p>How much does it cost?</p>
        <span style="margin-bottom:30px; margin-top:-5px">Free!</span>
<br><br>
        <p>Who are the coaches?</p>
        <span style="margin-bottom:30px; margin-top:-5px">Highly experienced, expert players and pros. <a href="/coaches" target="_blank" ><u>Learn more about our coaches here</u></a></span> 
<br><br>
		<p>What's a session like?</p>
        <span style="margin-bottom:30px; margin-top:-5px">Your coaching session is an on-demand chat and gaming experience with your coach. Ask him whatever your want to know, tell him what problems you're having or what goals/rank/level you want to achieve. Our excellent coaches use a wide range of tools and strategies to get you where you want to be quickly and effectively.</span>
<br><br>
        <p>Contact Info?</p>
        <span style="margin-bottom:30px; margin-top:-5px">Email us at <a href="mailto:coach@microcoaching.net" target="_blank">coach@microcoaching.net</a>, message on <a href="http://www.facebook.com/microcoach" target="_blank">facebook</a>, or hit us up on <a href="http://www.twitter.com/microcoaching_" target="_blank">twitter</a></span>
<br><br>
        <p>I want to coach - where do I apply?</p>
        <span style="margin-bottom:30px; margin-top:-5px">So you want to make money and help other gamers? Awesome! <a href="/becomeacoach"><u>Click here to start your application</u></a>. We'll be in touch ASAP.
        </span>
       
    </div>
</div>
</div>
</section>


   <?php 

include("footer.php"); 
?>
