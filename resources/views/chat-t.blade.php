@extends('layout.master')
@section('title', 'Chat with A Coach')

@section('scripts')
	<script src="//code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="{{ asset('js/testimonials.js') }}"></script>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-115361507-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-115361507-1');
	</script>

    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular-cookies.js"></script>
    
    <script src="/js/app.js"></script>
    <script src="/js/chat-service.js"></script>
    <script src="/js/chat-controller.js"></script>

    <script type="text/javascript">
        window._chatlio = window._chatlio||[];

        !function(){ var t=document.getElementById("chatlio-widget-embed");if(t&&window.ChatlioReact&&_chatlio.init)return void _chatlio.init(t,ChatlioReact);for(var e=function(t){return function(){_chatlio.push([t].concat(arguments)) }},i=["configure","identify","track","show","hide","isShown","isOnline", "page", "open", "showOrHide"],a=0;a<i.length;a++)_chatlio[i[a]]||(_chatlio[i[a]]=e(i[a]));var n=document.createElement("script"),c=document.getElementsByTagName("script")[0];n.id="chatlio-widget-embed",n.src="https://w.chatlio.com/w.chatlio-widget.js",n.async=!0,n.setAttribute("data-embed-version","2.3");
            n.setAttribute('data-widget-options', '{"embedInline": true}');
            n.setAttribute('data-widget-id','7311895d-b3c8-4f9e-6331-019b3318659f');
            c.parentNode.insertBefore(n,c);
        }();

        _chatlio.identify('{{ Auth::user()->id }}', {
            name: '{{ Auth::user()->name }}',
            email: '{{ Auth::user()->email }}',
            credits: '{{ Auth::user()->credits }}'
        });

        document.addEventListener("chatlio.online", function(event) {
            angular.element(document.querySelector('body')).scope().online = true;
        });

        document.addEventListener("chatlio.messageReceived", function(event) {
            angular.element(document.querySelector('body')).scope().setAnsweredOnce();
        });

        document.addEventListener("chatlio.conversationEnded", function(event){
            angular.element(document.querySelector('body')).scope().stopCounter();
            // document.location.href = '/router';
        });
    </script>
    
@stop

@section('ng-controller', 'chat')

@section('content')
    <section ng-init="user = {{ Auth::user() }}">
        <div>
            <p>Credits left: @{{ user.credits }}</p>
        </div>
        <div id="chatlioWidgetPlaceholder" style="margin: auto; height: 400px; width:100%;"></div>
    </section>
@stop