<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="description" content="<?php

if(isset($metaD) && !empty($metaD)) { 
   echo $metaD; 
} 
else { 
   echo "on demand video game coaching, league of legends coaching, csgo coaching"; 
} ?>" />

<title><?php 
if(isset($title) && !empty($title)) { 
   echo $title; 
} 
else { 
   echo "Microcoaching.net - On Demand Video Game Coaching"; 
} ?></title>

	<!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <!-- <link href="css/bootstrapTheme.css" rel="stylesheet"> -->
<!--<link href="css/custom.css" rel="stylesheet">-->
<link href="css/custom2.css" rel="stylesheet">
    <!--<link href="css/owl.carousel.css" rel="stylesheet">-->
    <!--<link href="css/owl.theme.css" rel="stylesheet">-->

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->   
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<link rel='shortcut icon' type='image/x-icon' href='/favicon.ico' />


</head><!--/head-->



<header id="header">
    <div class="header-wrapper">
          <nav class="navbar navbar-inverse" role="banner">
              <div class="container">
			    <div class="header-menu">
				  <div class="navbar-header">
				  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				  <span class="sr-only">Toggle navigation</span>
				  <span class="icon-bar"></span>
				  <span class="icon-bar"></span>
				  <span class="icon-bar"></span>
				  </button>
  <div class="logo-box navbar-left"><a class="navbar-brand" href="/"><img src="images/MC-LOGO-large.png" alt="microcoaching logo" class="img-responsive logosize" ></a></div>

				 <!--div class="logo-box navbar-left"><a class="navbar-brand" href="index.php"><img src="images/sidekick3.png" alt="sidekick - powered by microcoaching" class="img-responsive sidekick-logosize" ></a></div--> 
				  </div>
				  <div class="collapse navbar-collapse navbar-right mymenu">
				  <ul class="nav navbar-nav" style="padding-bottom:0px;">
			<li class="hidesmall" style="text-align: center;padding-bottom:0px;"><a href="http://www.twitter.com/microcoaching_" style="margin-right:10px; font-size:16px; margin-top:1px;"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
      
      <li class="hidesmall" style="text-align: center;padding-bottom:0px;"><a href="http://www.facebook.com/microcoach" style="margin-right:10px; font-size:16px; margin-top:1px;"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
          
          <li style="text-align: center;padding-bottom:0px;" <?php if(basename($_SERVER['PHP_SELF']) == "about.php") { echo "class='active'"; } ?>>
 
      <a href="/about" style="font-size:16px; margin-top:1px;">ABOUT</a>
      </li>
 
   <li style="text-align: center;padding-bottom:0px;" <?php if(basename($_SERVER['PHP_SELF']) == "coaches.php") { echo "class='active'"; } ?>>
 
      <a href="/coaches" style="font-size:16px; margin-top:1px;">OUR COACHES</a>
      </li>
 
          <li style="text-align: center;padding-bottom:0px;" <?php if(basename($_SERVER['PHP_SELF']) == "ourtools.php") { echo "class='active'"; } ?>>
      <a href="/ourtools" style="font-size:16px; margin-top:1px;">TOOLS</a>
      </li>
     
      <li style="text-align: center;padding-bottom:0px;" <?php if(basename($_SERVER['PHP_SELF']) == "becomeacoach.php") { echo "class='active'"; } ?>>
      <a href="/becomeacoach" style="margin-right:10px; font-size:16px; margin-top:1px;">BECOME A COACH</a>
      </li>
         
        <!-- <li <?php if(basename($_SERVER['PHP_SELF']) == "sidekick.php") { echo "class='active'"; } ?>><a href="/sidekick.php" class="btn btn-secondary login" style="font-size:22px; text-align: center;padding-bottom:0px;margin-bottom: 28px;">TRY NOW!</a></li>-->
          
          </ul>
          </div>
        </div>
              </div> 
          </nav>
    </div>


</header>

<!---end of headerphp-->

<body>
<?php include_once("analyticstracking.php") ?>