<div>
    <div style="display: flex; color: #999; margin: 1em 0;">
        <hr style="width: 100%; background: #999; border: none; height: 1px">
        <span style="margin: 0 1em;">or</span>
        <hr style="width: 100%; background: #999; border: none; height: 1px">
    </div>

    <div>
        <a href="{{ url('login/facebook') }}" class="button facebook">Continue with Facebook</a>
        <a href="{{ url('login/google') }}" class="button google">Continue with Google</a>
    </div>

    <div style="margin-top: 1.5em;">
        <p style="font-size: 14px; text-align: center; color: #999;">By signing in through social media links you accept the&nbsp;<a href="{{ route('terms') }}">Terms and Conditions</a> and <a href="{{ route('privacy') }}">Privacy Policy</a>.</p>
    </div>
</div>